function [ ] = mtv_correct_receive_profile( fname_M0, fname_T1, fname_cord, fname_CSF, min_lim_for_box, overlap, boxsize )
% This function correct M0 for the receive profile by fitting many local
% boxes and then, joining them together to build the PD map. It finally
% normalized it by CSF and compute MTV.

%   To run it, you need to add these git repositories to your matlab path.
%   you can do it by:  addpath(genpath('gitPath'));
%   mrQ - https://github.com/mezera/mrQ
%   Vistasoft - https://github.com/vistalab/vistasoft 
%   KNKUtils (from Kendrick Kay) - https://github.com/kendrickkay/knkutils 
%   You also need to install SPM.

% Adapted by Simon LEVY (NeuroPoly, Copyright (c) 2015 Polytechnique
% Montreal <www.neuro.polymtl.ca>) from Aviv Mezer (C) Hebrew University,
% 2015

%% Default
% minimal limits to keep a box
if ~exist('min_lim_for_box','var') || isempty(min_lim_for_box)
    min_lim_for_box = [0.2, 20]; % [% of voxels in the box, minimal Nb of voxel in the box]
end
% overlap between boxes
if ~exist('overlap','var') || isempty(overlap)
    overlap = 0.5; % overlap between boxes
end
% box size
if ~exist('boxsize','var') || isempty(boxsize)
    boxsize=[10,10,50]; % in mm
end


%%
% path to the input nifti files

BMfile=fname_cord; % mask file where you got data to fit PD with reliable T1
CSFfile=fname_CSF;
T1file=fname_T1;
M0file=fname_M0;
outDir =['receive_profile_correction_',datestr(now,'yyyymmdd_HHMMSS')] %output directory path
dir='./' %the top lever dir


%%
%% run the fit number of input coils --> in our case 1.
Coilsinfo.maxCoil=1;Coilsinfo.minCoil=1; Coilsinfo.useCoil=1;
Reg=5;Init=5; % The method of fitting. in our case No 5. this is the local T1.
 
%%
% Make an output directory
if ~exist(outDir,'dir'); mkdir(outDir); end
% MAke a output structure.
mrQ = mrQ_Create(dir,[],outDir);

%  We are using code for mrQ software. The code also fit B1 and T1 etc. 
%   Let's skip all irrelevant mrQ processing until the PD and coil
% sensitivity mapping. So we define some bookkeeping parameters manually.
mrQ.Arange_Date=date;
mrQ.SEIR_done=1;
mrQ.SPGR_init_done=1;
mrQ.SPGR_coilWeight_done=1;
mrQ.SPGR_T1fit_done=1;
mrQ.segmentaion=1;
mrQ.calM0_done=1;
mrQ.calM0_done=1;
mrQ.brakeAfterPD=1;
mrQ.SunGrid=0;
mrQ.proclus=0;
mrQ.spgr_initDir=outDir;
 
% Let's save the M0 image
mrQ.M0combineFile=M0file;
% Same 
save(mrQ.name,'mrQ');
 
 
%% Get all the fit parameters
% Generate the details for the local Gain (and PD) fit. The idea is that
% the M0 is separated to many overlap areas. We use here default parameters
% box-size (size of local area we fit = 14mm)  and resolution (the image
% resolution ?it is possible to bluer it (to fit the sensitivity ), overlap
% between each local fit area = 50%). it might be useful to optimize that.

% boxsize=[];
% outMm=[2 2 2]% We replace the data to this low resolution for speed. since we only fit smooth gain any way. you can change it. To  keep the original resolution change the input to  outMm=0;
outMm=0;
[mrQ.opt_logname]=mrQ_PD_multicoil_RgXv_GridCall_Fittesting(mrQ.spgr_initDir,mrQ.sub,mrQ.PolyDeg,M0file,T1file,BMfile,outMm,boxsize,overlap,Coilsinfo,Reg,Init,[],mrQ);
%# perform k-means clustering on T1 map in the defined brain mask, using 3 clusters and excluding voxels with T1 higher than 1/0.35(=2.9)s. Boxes are defined according to 'boxsize' and voxel dimensions of the T1 data.
save(mrQ.name,'mrQ');
%%
% fit each local M0 area (with T1 input). 
mrQ_fitM0boxesCall_T1PD(mrQ.opt_logname,min_lim_for_box)
%# It seems that only a few boxes are used (for the cord) because of the small size of the cord
%% Build the local fits
%Join the local overlap area to one PD image.
mrQ.opt=mrQ_buildPD_ver2(mrQ.opt_logname,CSFfile,[mrQ.outDir '/R1_seg.nii.gz'],[],[],0.01,min_lim_for_box);

%% Compute MTV from the WF_map
WF_map = load_nii_data(mrQ.opt.WFfile);
MTV_map = ones(size(WF_map))-WF_map;
save_nii_v2(MTV_map, 'mtv.nii.gz', fname_CSF, 64);
copyfile(mrQ.opt.WFfile, './wf.nii.gz');
copyfile(mrQ.opt.Gainfile, './b1minus.nii.gz');

end

