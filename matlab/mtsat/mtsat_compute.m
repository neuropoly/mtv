function [ ] = mtsat_compute(PDw, T1w, MTw, alpha_PD, alpha_T1, alpha_MT, TR_PD, TR_T1, TR_MT, output_fname_MTsat, output_fname_T1, path_T1_map_input)
% Compute MT saturation map from a PD-weigthed, a T1-weighted and a MT-weighted FLASH images
% according to Helms et al., MRM, 60:1396?1407 (2008) and equation erratum in MRM, 64:1856 (2010).
%   First 3 arguments are (becareful to respect the order): the PD-weighted image, the T1-weighted
%   image and the MT-weigthed image.
%   Next 3 arguments are the corresponding flip angles used for the acquisition of each image (in
%   the same order).
%   Next 3 arguments are the corresponding TR used for the acquisition of each image (in the same
%   order).
%   Next 2 aguments are the names of the output nifti files for the MT saturation map and the T1 map
%   in this order (optional arguments, defaults ='MTsat.nii.gz','T1.nii.gz')
%   Last argument is the path to a previously computed T1 map to give in input (optional argument).
%   If no T1 map is given in input, this function outputs it. If it is given in input, this function
%   uses it to compute the MT saturation map.

% set default output files name
if ~exist('output_fname_MTsat','var') || isempty(output_fname_MTsat)
    output_fname_MTsat = 'MTsat.nii.gz';
end
% set default output files name
if ~exist('output_fname_T1','var') || isempty(output_fname_T1)
    output_fname_T1 = 'T1.nii.gz';
end

% Load nii
PDw_data = load_nii_data(PDw);
PDw_data = double(PDw_data); % convert data coding to double

T1w_data = load_nii_data(T1w);
T1w_data = double(T1w_data); % convert data coding to double

MTw_data = load_nii_data(MTw);
MTw_data = double(MTw_data); % convert data coding to double

% Convert angles into radians
display('Flip angles in radians are:')
alpha_PD = (pi/180)*alpha_PD
alpha_T1 = (pi/180)*alpha_T1
alpha_MT = (pi/180)*alpha_MT

% check if a T1 map was given in input; if not, compute it
if ~exist('path_T1_map_input','var') || isempty(path_T1_map_input)
    % compute R1
    R1 = 0.5*((alpha_T1/TR_T1)*T1w_data - (alpha_PD/TR_PD)*PDw_data)./(PDw_data/alpha_PD - T1w_data/alpha_T1);
else
    T1_map_data = load_nii_data(path_T1_map_input);
    T1_map_data = double(T1_map_data); % convert data coding to double
    R1 = 1./T1_map_data;
end

% compute A
A = (TR_PD*alpha_T1/alpha_PD - TR_T1*alpha_PD/alpha_T1)*((PDw_data.*T1w_data)./(TR_PD*alpha_T1*T1w_data - TR_T1*alpha_PD*PDw_data));
% compute MTsat
MTsat = TR_MT*(alpha_MT*(A./MTw_data) - ones(size(MTw_data))).*R1 - (alpha_MT^2)/2;

% output MTsat and T1 maps in nii
save_nii_v2(MTsat, output_fname_MTsat, T1w, 64);
if ~exist('path_T1_map_input','var') || isempty(path_T1_map_input)
   save_nii_v2(1./R1, output_fname_T1, T1w, 64);
end


end

