function [  ] = mtv_compute_m0_t1_nii( fname_spgr, flip_angles, tr, fname_output, fname_b1, verbose )
%UNTITLED3 Summary of this function goes here
%   fname_spgr: list of strings (increasing order of flip angles)
%   flip_angles: list of int
%   tr: float (in seconds)
%   fname_output: list of strings (M0, T1, MTV)
%   fname_b1: string

%% Load data
% SPGR
nb_flip_angles = length(flip_angles);
spgr1_nii = load_nii(char(fname_spgr(1)));
spgr1_nii.hdr.dime.datatype = 64; spgr1_nii.hdr.dime.bitpix = 64; % set header datatype to float
spgr = double(spgr1_nii.img);
for i_fa=2:nb_flip_angles
    nii = load_nii(char(fname_spgr(i_fa)));
    spgr=cat(4,spgr, double(nii.img));
end

% b1
if exist('fname_b1','var')
    nii = load_nii(fname_b1);
    b1 = nii.img;
else
    b1 = [];
end

% verbose
if ~exist('verbose', 'var')
    verbose = 0;
end

%% Estimate M0 and T1 fitting SPGR data
m0_nii = spgr1_nii;
t1_nii = spgr1_nii;
[m0_nii.img, t1_nii.img] = mtv_compute_m0_t1 (spgr, flip_angles, tr, b1, [], 0);

%% Save data as niftii files
save_nii(m0_nii, char(fname_output(1)));
save_nii(t1_nii, char(fname_output(2)));


end

