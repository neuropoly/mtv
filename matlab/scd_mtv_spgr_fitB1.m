function [B1, resnorm]=scd_mtv_spgr_fitB1(T1, M0, S, flipAngles, TR, roi)

if nargin<6, roi=ones(size(T1)); end
B1=zeros([size(T1,1),size(T1,2),size(T1,3), 2]); resnorm=zeros(size(T1));
dims=size(B1);
j_progress('loop over voxels...')
S=double(S);
maxS=max(S(:));
for iz=1:size(T1,3)
    for iy=1:size(T1,2)
        for ix=1:size(T1,1)
            if roi(ix,iy,iz) && T1(ix,iy,iz,1)~=0
                j_progress(((iz-1)*dims(2)*dims(3)+(iy-1)*dims(3)+ix)/(dims(1)*dims(2)*dims(3)))
                x0=[1, double(M0(ix,iy,iz,1))]; lb=[0, 0]; ub=[2 maxS];
                [B1(ix,iy,iz,:) resnorm(ix,iy,iz)]=fminunc(@(x) norm(model_error(x,T1(ix,iy,iz,1), squeeze(S(ix,iy,iz,:)), flipAngles, TR)),x0);

               [~, model]=model_error(B1(ix,iy,iz,:), T1(ix,iy,iz,1),  squeeze(S(ix,iy,iz,:)), flipAngles, TR);
               plot(flipAngles,model,'r+',flipAngles,squeeze(S(ix,iy,iz,:)),'b+'); drawnow;
                %disp(['S = ' num2str(squeeze(S(ix,iy,iz,:))') ', model= ' num2str(model(:)') ', error= ' num2str(error(:)')]);
                
            end
        end
    end
end
j_progress('done...')


function [error, model]=model_error(B1M0, T1, S, flipAngles, TR)
B1=B1M0(1); M0=B1M0(2);
model=M0.*(1-exp(-TR./T1))./(1-exp(-TR./T1).*cos(flipAngles*pi/180*B1)).*sin(flipAngles*pi/180*B1);
%plot(flipAngles,model,'r+',flipAngles,S,'b+'); drawnow;
error= S(:)-model(:);
