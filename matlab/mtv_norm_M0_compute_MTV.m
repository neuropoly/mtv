function [PD, MTV]=mtv_norm_M0_compute_MTV(M0,maskcsf, verbose)
% Normalize M0 by the CSF and compute MTV.

if nargin<3, verbose=1; end

M0=squeeze(M0(:,:,:,1));

PD=zeros(size(M0));

% [M0iZ_incsf_median_reg, M0iZ_incsf_median] = scd_display_intensitySbS(M0,maskcsf);
mean_std_median_SbS=compute_metric_mean_std_median_SbS(M0,maskcsf);
% p = polyfit(2:(size(M0,3)-1),mean_std_median_SbS(2:(size(M0,3)-1),3)',2);
% fitted_median_PD_in_CSF = polyval(p,1:size(M0,3));
fitted_median_PD_in_CSF = mean(mean_std_median_SbS(3:end-3,3))*ones(size(M0,3),1);

if verbose
    h=figure;
    hold on
    plot(1:size(M0,3),mean_std_median_SbS(:,3))
    plot(1:size(M0,3),fitted_median_PD_in_CSF)
    legend('Median M0 in CSF','Corrected median M0 in CSF')
    hold off
    basename_fig = 'M0_normalization_by_CSF';
    filename_fig = 'M0_normalization_by_CSF';
    nb = -1;
    while exist([filename_fig '.jpg'],'file')
        nb = nb+1;
        filename_fig = [basename_fig num2str(nb)];
    end
    saveas(h,filename_fig,'jpg')
end

    
for iz=1:size(M0,3)
    PD(:,:,iz)=M0(:,:,iz)/fitted_median_PD_in_CSF(iz);
end

% compute MTV from PD
MTV=ones(size(M0))-PD;

if verbose
    figure
    scd_display_3Dmatrix(PD, [0 1.5])
end