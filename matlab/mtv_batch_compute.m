%% input param
SPGR=load_nii('SPGRs.nii'); %SPGR.img=double(SPGR.img)./repmat(reshape([80 41 23 17],[1 1 1 4]),[SPGR.dims(1:3)]);
flipAngles=[5 10 20 30];
TR=0.010;
b1Map=load_nii('b1_reslice.nii');
b1Map.img=mtv_fit3dpolynomialmodel(b1Map.img,b1Map.img>0.5,3);
save_nii_v2(b1Map,'b1_smooth')
seg='bottle_mask.nii.gz';

%% Correct B1 (facultative : correct asymetry in SC)
seg_nii=load_nii(seg);
B1iz=scd_stat_roi_mean_std(b1Map.img,seg_nii.img);
figure, plot(B1iz,'+'); hold on;
B1iz=polyval(polyfit(find(B1iz),B1iz(find(B1iz)),1),1:length(B1iz));
plot(B1iz,'r'); hold off;
dims=size(b1Map.img);
B1_corr=repmat(reshape(B1iz,1,1,length(B1iz)),dims(1),dims(2));
b1Map.img=B1_corr;
%% Launch MTV
[M0, t1Biased] = mtv_compute_m0_t1(double(SPGR.img), flipAngles, TR, b1Map.img); %,[],0,1);
save_nii_v2(M0(:,:,:,1),'M0','SPGRs.nii.gz')
save_nii_v2(t1Biased(:,:,:,1),'T1','SPGRs.nii.gz',64)

%% create csf mask
% csf mask using T1 values
csf_mask=t1Biased(:,:,:,1);
csf_mask(csf_mask<3 | csf_mask>7)=0;
% csf mask using seg
seg_nii=load_nii(seg);
SE=translate(strel('disk',floor(4/seg_nii.scales(1)),4),[0 3]);
seg_csf=logical(imdilate(seg_nii.img,SE)-seg_nii.img);
% compose
seg_csf=csf_mask & seg_csf;
figure,imagesc3D(seg_csf)
save_nii_v2(seg_csf,'CSF_seg',seg);
%% redo fitting in csf using T1=4.5s and compute M0csf
seg_csf=load_nii_data('CSF_seg.nii');
M0csf = mtv_compute_m0_t1(double(SPGR.img), flipAngles, TR, double(b1Map.img),seg_csf,4);

[M0csfiZ, ~, wgts]=scd_stat_roi_mean_std(M0csf,seg_csf);

% fit
ft = fittype( 'poly2' );
opts = fitoptions( 'Method', 'LinearLeastSquares' );
opts.Robust = 'Bisquare';
wgts(M0csfiZ==0)=0; wgts(isnan(M0csfiZ))=[]; 
opts.Weights = wgts;

% Fit model to data.
[xData, yData] = prepareCurveData( [], M0csfiZ );
fitresult = fit( xData, yData, ft, opts );

% Plot fit with data.
figure( 'Name', 'untitled fit 1' );
h = plot( fitresult, xData, yData );
legend( h, 'M0csfiZ', 'untitled fit 1', 'Location', 'NorthEast' );
% Label axes
ylabel( 'M0csfiZ' );
grid on
set(h,'MarkerSize',40)

% Evaluate
M0csfiZfit = feval(fitresult,1:length(M0csfiZ));

%% MTV to PD
dims=size(M0);
PD=zeros(dims(1:3));
for iz=1:size(M0,3)
    PD(:,:,iz)=M0(:,:,iz,1)/M0csfiZfit(iz);
end
save_nii_v2(1-PD,'mtv','SPGRs.nii.gz',64)

%[M0csf_median, M0csf_std]=scd_stat_roi_mean_std(M0(:,:,:,1),seg_csf)

[M0cordiZ, M0cord_std]=scd_stat_roi_mean_std(M0(:,:,:,1),seg_nii.img)

figure
plot(M0cordiZ,'r+')
hold on
plot(M0csfiZfit,'b+')

figure
plot(1-M0cordiZ(:)./M0csfiZfit,'b+')
ylim([0 1])


%% correct reception profile
mtv_correct_receive_profile M0.nii T1.nii SC_seg.nii.gz CSF_seg.nii
mtv_cor=mtv.*mean(mtv(seg_nii.img & ~~b1Map.img))./mtv_fit3dpolynomialmodel(load_nii_data('mtv.nii.gz'),seg_nii.img & ~~b1Map.img,1);
mtv_cor(~seg_nii.img)=mtv(~seg_nii.img);
save_nii_v2(mtv_cor,'mtv_cor','mtv.nii.gz')
%k=fminsearch(@(x) std(M0(logical(seg_csf))./b1Map.img(logical(seg_csf)).^x)/mean(M0(logical(seg_csf))./b1Map.img(logical(seg_csf)).^x), 2)