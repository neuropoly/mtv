function [ signal ] = signal_ss_rf_spoiled_gre( T1, T2, fa, TR, phi )
% Compute the unitary signal (normalized by PD*gain*exp(-TE/TR)) signal from steady-state RF spoiled gradient echo sequence. 
%   fa: nominal flip angle (in radians)
%   phi: RF phase difference increment (in radians)

% % convert flip angle and RF phase difference increment to radians
% fa=deg2rad(fa);
% phi=deg2rad(phi);

% compute lambda
lbda=lambda_contfract(TR,fa,T1,T2,phi);
% compute conjugate
lbda_conj=conj(lbda);

E1=exp(-TR/T1);
D=1-E1*cos(fa)-0.5*(1-cos(fa))*(1+E1)*(lbda+lbda_conj)+(E1-cos(fa))*(abs(lbda)^2);
% signal expression within a multiplicative constant
signal=abs((1-E1)*(1/D)*sin(fa)*(1-lbda_conj));

end

