function [ T1_map, convergence_map, M0_map ] = mtv_compute_M0_T1_from_SPGR_ss_eq( fname_spgr1, fname_spgr2, fa1, fa2, phi, T2_list, fname_masks, TR, fname_b1, TE, T2star_list )
%[ T1_map, convergence_map, M0_map ] = mtv_compute_M0_T1_from_SPGR_ss_eq( fname_spgr1, fname_spgr2, fa1, fa2, phi, T2_list, fname_masks, TR, fname_b1, TE, T2star_list )
%   fa1, fa2: nominal flip angles (in degrees) used to get spgr1 and spgr2
%   respectively.
%   phi: RF phase increment (in degrees) used for spoiling during
%   acquisition of SPGRs.
%   T2: list of guesses of the transversal relaxation time of the tissue
%   (in s) for each mask given in fname_masks
%   TR: Repetition time (in s)
%   TE: Echo Time (in s)
%   T2star_list: list of guesses of the T2* of the tissue (in s) for each
%   ROI given in fname_mask

% Load data
spgr1 = load_nii_data(fname_spgr1);
spgr1 = double(spgr1); % convert data coding to double
spgr2 = load_nii_data(fname_spgr2);
spgr2 = double(spgr2); % convert data coding to double
b1 = load_nii_data(fname_b1);
% Number of different ROIs
nb_T2=length(T2_list); nb_ROI=length(fname_masks); nb_T2star=length(T2star_list);
if ~(nb_T2==nb_ROI && nb_T2star==nb_ROI)
    error('ERROR: Number of specified T2 or T2* guesses and ROIs are different.');
end
for i_ROI=1:nb_ROI
    ROIs_data{i_ROI}=load_nii_data(char(fname_masks(i_ROI)));
end

% Convert angles to radians
fa1=degtorad(fa1);fa2=degtorad(fa2);phi=degtorad(phi);

% Initialization
T1_map = spgr1;
M0_map = spgr1;
convergence_map = spgr1;
T2_default=0.073;
T2=T2_default; % in the case of a voxel outside of every ROI (value for lateral columns in Smith et al. 2008)
T2star_default=0.050; % in the case of a voxel outside of every ROI (approximate value for white matter, see Wansapura et al. (1999) and P�ran et al. (2007))
T2star=T2star_default;

% Solve equation numerically for all voxels
nx=size(spgr1,1);ny=size(spgr1,2);nz=size(spgr1,3);
disp('Loop over voxels:       ')
tic
for mm=1:nx
    for nn=1:ny
        for pp=1:nz
            fprintf('\b\b\b\b\b\b\b%3i%%...',floor(100*((mm-1)*nz*ny+(nn-1)*nz+pp)/(nx*ny*nz)))
            %num2str(100*((pp-1)*(ny-1)*(nx-1) + (nn-1)*(nx-1) + mm) / ((nz-1)*(ny-1)*(nx-1)))),'%'])
            % adjust the T2 guess according to the ROI we are in (assume
            % that it doesn't exist voxels included in more than one mask)
            for i_ROI=1:nb_ROI
                if ROIs_data{i_ROI}(mm,nn,pp)==1
                    T2=T2_list(i_ROI);
                    T2star=T2star_list(i_ROI);
                end
            end
            
            % solve steady-state RF spoiling equation
            ratio_actual=spgr1(mm,nn,pp)/spgr2(mm,nn,pp);
            [T1,fval,exitflag] = fminsearch(@(T1) diff_signal_ratios_actual_theo(T1, T2, fa1*b1(mm,nn,pp), fa2*b1(mm,nn,pp), TR, phi, ratio_actual),0.8,optimset('Display','off','MaxFunEvals',10^15,'TolFun',10^(-20),'TolX',10^(-10)));
%             [T1,fval,exitflag,output] = fzero(@(T1) diff_signal_ratios_actual_theo(T1, T2, fa1*b1_nii.img(mm,nn,pp), fa2*b1_nii.img(mm,nn,pp), TR, phi, ratio_actual),0.9,optimset('Display','off','TolX',10^(-10)));
            if T1<0
                T1=0;
            elseif T1>10
                T1=10;
            end
            T1_map(mm,nn,pp)=T1;
            convergence_map(mm,nn,pp,1)=T1;
            convergence_map(mm,nn,pp,2)=fval;
            convergence_map(mm,nn,pp,3)=exitflag;
            % compute M0
            M0_from_spgr1=get_M0_from_T1_spgr_eq( spgr1(mm,nn,pp), T1, T2, fa1*b1(mm,nn,pp), TR, phi, TE, T2star );
            M0_from_spgr2=get_M0_from_T1_spgr_eq( spgr2(mm,nn,pp), T1, T2, fa2*b1(mm,nn,pp), TR, phi, TE, T2star );            
            M0_map(mm,nn,pp)=mean([M0_from_spgr1,M0_from_spgr2]);            

            T2=T2_default;
            T2star=T2star_default;
        end
    end
end
disp('...done.')
elapsed_time = toc;
fprintf('%d minutes and %f seconds\n',floor(elapsed_time/60),rem(elapsed_time,60));

% Save computed maps as NIFTI files
save_nii_v2(T1_map,'T1.nii.gz',fname_spgr2,64);
save_nii_v2(convergence_map,'convergences_indexes.nii.gz',fname_spgr2,64);
save_nii_v2(M0_map,'M0.nii.gz',fname_spgr2,64);

end

