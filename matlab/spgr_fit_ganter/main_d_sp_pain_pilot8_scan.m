%% Parameters of the FLASH acquisitions
fname_spgr1='spgr4to10.nii.gz';
fname_spgr2='spgr20to10.nii.gz';
fa1=4; % flip angle 1 (in degrees)
fa2=20; % flip angle 2 (in degrees)
phi=50; % linear phase increment used for spoiling (in degrees)
T2_list=[2.5,0.073]; % guess of the transversal relaxation time in the tissue (in s)
fname_masks={'spgr20to10_csf_mask.nii.gz','spgr20to10_seg.nii.gz'};
TR=0.035; % Repetition Time (in s)
fname_b1='b1_scaling_map_fv_used.nii.gz';
TE=0.00592;
T2star_list=[0.120,0.050]; % guess of T2* in the tissue (in s) [see Wansapura et al. (1999) and P�ran et al. (2007)]

%% Compute T1 map
[ T1_map, convergence_map, M0_map ] = compute_t1_from_spgr_eq( fname_spgr1, fname_spgr2, fa1, fa2, phi, T2_list, fname_masks, TR, fname_b1, TE, T2star_list );