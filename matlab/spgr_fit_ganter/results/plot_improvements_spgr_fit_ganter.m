%% Load data
T1_fram_nii=load_nii('/Volumes/slevy-5/data/criugm/d_sp_pain_01/mtv/T1_Fram_hdr_from_spgr20crop.nii.gz');
T1_ganter_nii=load_nii('/Volumes/slevy-5/data/criugm/d_sp_pain_01/mtv/T1_hdr_from_spgr20crop.nii.gz');
MTV_fram_nii=load_nii('/Volumes/slevy-5/data/criugm/d_sp_pain_01/mtv/mtv_Fram_hdr_from_spgr20crop.nii.gz');
% M0_ganter_without_T2star_nii=load_nii('M0_without_T2star.nii.gz');
MTV_ganter_nii=load_nii('/Volumes/slevy-5/data/criugm/d_sp_pain_01/mtv/mtv_hdr_from_spgr20crop.nii.gz');

seg_nii=load_nii('/Volumes/slevy-5/data/criugm/d_sp_pain_01/mtv/label/template/MNI-Poly-AMU_WM.nii.gz');
csf_mask_nii=load_nii('/Volumes/slevy-5/data/criugm/d_sp_pain_01/mtv/spgr20crop_csf.nii.gz');

% %% Estimate metric slice-by-slice:
% % - in the cord
% T1_fram_SbS=compute_metric_mean_std_median_SbS(T1_fram_nii.img,seg_nii.img);
% T1_ganter_SbS=compute_metric_mean_std_median_SbS(T1_ganter_nii.img,seg_nii.img);
% M0_fram_SbS=compute_metric_mean_std_median_SbS(MTV_fram_nii.img,seg_nii.img);
% M0_ganter_without_T2star_SbS=compute_metric_mean_std_median_SbS(M0_ganter_without_T2star_nii.img,seg_nii.img);
% M0_ganter_with_T2star_SbS=compute_metric_mean_std_median_SbS(MTV_ganter_with_T2star_nii.img,seg_nii.img);
% % - in the CSF
% T1_fram_SbS_in_CSF=compute_metric_mean_std_median_SbS(T1_fram_nii.img,csf_mask_nii.img);
% T1_ganter_SbS_in_CSF=compute_metric_mean_std_median_SbS(T1_ganter_nii.img,csf_mask_nii.img);
% M0_fram_SbS_in_CSF=compute_metric_mean_std_median_SbS(MTV_fram_nii.img,csf_mask_nii.img);
% M0_ganter_without_T2star_SbS_in_CSF=compute_metric_mean_std_median_SbS(M0_ganter_without_T2star_nii.img,csf_mask_nii.img);
% M0_ganter_with_T2star_SbS_in_CSF=compute_metric_mean_std_median_SbS(MTV_ganter_with_T2star_nii.img,csf_mask_nii.img);
% 
% %% Plot
% nb_slice=size(T1_fram_nii.img,3);
% figure
% 
% subplot(1,2,1)
% hold on
% plot(0:(nb_slice-1), T1_fram_SbS(:,1),'r.-','MarkerSize',25)
% plot(0:(nb_slice-1), T1_ganter_SbS(:,1),'b.-','MarkerSize',25)
% plot(0:(nb_slice-1), T1_fram_SbS_in_CSF(:,1),'ro-','MarkerSize',10)
% plot(0:(nb_slice-1), T1_ganter_SbS_in_CSF(:,1),'bo-','MarkerSize',10)
% hold off
% legend('cord: equation Fram 1987','cord: equation Ganter 2006','CSF: equation Fram 1987','CSF: equation Ganter 2006')
% title('T1')
% xlabel('Slice # (0=most caudal)'); ylabel('Mean');
% grid on
% grid minor
% set(gca,'FontSize',20);
% 
% subplot(1,2,2)
% hold on
% plot(0:(nb_slice-1), M0_fram_SbS(:,1),'r.-','MarkerSize',25)
% plot(0:(nb_slice-1), M0_ganter_without_T2star_SbS(:,1),'b.-','MarkerSize',25)
% plot(0:(nb_slice-1), M0_ganter_with_T2star_SbS(:,1),'b--.','MarkerSize',25)
% plot(0:(nb_slice-1), M0_fram_SbS_in_CSF(:,1),'ro-','MarkerSize',10)
% plot(0:(nb_slice-1), M0_ganter_without_T2star_SbS_in_CSF(:,1),'bo-','MarkerSize',10)
% plot(0:(nb_slice-1), M0_ganter_with_T2star_SbS_in_CSF(:,1),'b--o','MarkerSize',10)
% hold off
% legend('cord: equation Fram 1987','cord: equation Ganter 2006 (without T2^*)','cord: equation Ganter 2006 (with T2^*)','CSF: equation Fram 1987','CSF: equation Ganter 2006 (without T2^*)','CSF: equation Ganter 2006 (with T2^*)')
% title('M0')
% xlabel('Slice # (0=most caudal)'); ylabel('Mean');
% grid on
% grid minor
% set(gca,'FontSize',20);

% histograms
% binarize white matter mask
WM = seg_nii.img>0.5;
CSF = logical(csf_mask_nii.img);

%Find the peak of each histogram for T1
[WM_values, WM_density]= ksdensity(T1_fram_nii.img(find(WM)), [min(T1_fram_nii.img(find(WM))):0.001:max(T1_fram_nii.img(find(WM)))] );
hist_peak_T1_Fram= WM_density(find(WM_values==max(WM_values)));% median(PD(find(CSF)));

[WM_values, WM_density]= ksdensity(T1_ganter_nii.img(find(WM)), [min(T1_ganter_nii.img(find(WM))):0.001:max(T1_ganter_nii.img(find(WM)))] );
hist_peak_T1_Ganter= WM_density(find(WM_values==max(WM_values)));% median(PD(find(CSF)));

[CSF_values, CSF_density]= ksdensity(T1_fram_nii.img(find(CSF)), [min(T1_fram_nii.img(find(CSF))):0.001:max(T1_fram_nii.img(find(CSF)))] );
hist_peak_T1_Fram_CSF= CSF_density(find(CSF_values==max(CSF_values)));% median(PD(find(CSF)));

[CSF_values, CSF_density]= ksdensity(T1_ganter_nii.img(find(CSF)), [min(T1_ganter_nii.img(find(CSF))):0.001:max(T1_ganter_nii.img(find(CSF)))] );
hist_peak_T1_Ganter_CSF= CSF_density(find(CSF_values==max(CSF_values)));% median(PD(find(CSF)));

%Find the peak of each histogram for MTV
[WM_values, WM_density]= ksdensity(MTV_fram_nii.img(find(WM)), [min(MTV_fram_nii.img(find(WM))):0.001:max(MTV_fram_nii.img(find(WM)))] );
hist_peak_MTV_Fram= WM_density(find(WM_values==max(WM_values)));% median(PD(find(CSF)));

[WM_values, WM_density]= ksdensity(MTV_ganter_nii.img(find(WM)), [min(MTV_ganter_nii.img(find(WM))):0.001:max(MTV_ganter_nii.img(find(WM)))] );
hist_peak_MTV_Ganter= WM_density(find(WM_values==max(WM_values)));% median(PD(find(CSF)));

[CSF_values, CSF_density]= ksdensity(MTV_fram_nii.img(find(CSF)), [min(MTV_fram_nii.img(find(CSF))):0.001:max(MTV_fram_nii.img(find(CSF)))] );
hist_peak_MTV_Fram_CSF= CSF_density(find(CSF_values==max(CSF_values)));% median(PD(find(CSF)));

[CSF_values, CSF_density]= ksdensity(MTV_ganter_nii.img(find(CSF)), [min(MTV_ganter_nii.img(find(CSF))):0.001:max(MTV_ganter_nii.img(find(CSF)))] );
hist_peak_MTV_Ganter_CSF= CSF_density(find(CSF_values==max(CSF_values)));% median(PD(find(CSF)));





% T1
close all;
f=figure;
% histyy(T1_fram_nii.img(logical(seg_nii.img)),50,T1_ganter_nii.img(logical(seg_nii.img)),50);

% WM
subplot(1,2,1)
histogram(T1_fram_nii.img(logical(WM)),60);
hold on;
histogram(T1_ganter_nii.img(logical(WM)),60);

y_lim = get(gca,'ylim');
plot([hist_peak_T1_Fram hist_peak_T1_Fram],y_lim, 'Color', 'b', 'LineWidth',3);
plot([hist_peak_T1_Ganter hist_peak_T1_Ganter],y_lim, 'Color', 'r', 'LineWidth',3);

hold off;
grid on; grid minor;
% legend('equation Fram 1987','equation Ganter 2006')
title('White matter')
xlabel('T_1 (s)'); ylabel('Number of voxels');
set(gca,'FontSize',35);
xlim([0.7 2.6]);

%CSF
subplot(1,2,2)
histogram(T1_fram_nii.img(CSF),60);
hold on;
histogram(T1_ganter_nii.img(CSF),60);

y_lim = get(gca,'ylim');
plot([hist_peak_T1_Fram_CSF hist_peak_T1_Fram_CSF],y_lim, 'Color', 'b', 'LineWidth',3);
plot([hist_peak_T1_Ganter_CSF hist_peak_T1_Ganter_CSF],y_lim, 'Color', 'r', 'LineWidth',3);

hold off;
grid on; grid minor;
% legend('equation Fram 1987','equation Ganter 2006')
title('CSF')
xlabel('T_1 (s)'); ylabel('Number of voxels');
set(gca,'FontSize',35);
xlim([0.7 10]);


saveas(f,'/Users/slevy_local/Dropbox/simon_levy/master/master_thesis/fig/graph_soutenance/comparison_Fram_Ganter_T1_WM_CSF_d_sp_pain_01.jpg')

% MTV
f=figure;
% histyy(T1_fram_nii.img(logical(seg_nii.img)),50,T1_ganter_nii.img(logical(seg_nii.img)),50);

subplot(1,2,1)
histogram(MTV_fram_nii.img(logical(WM)),60);
hold on;
histogram(MTV_ganter_nii.img(logical(WM)),60);

y_lim = get(gca,'ylim');
plot([hist_peak_MTV_Fram hist_peak_MTV_Fram],y_lim, 'Color', 'b', 'LineWidth',3);
plot([hist_peak_MTV_Ganter hist_peak_MTV_Ganter],y_lim, 'Color', 'r', 'LineWidth',3);


hold off;
grid on; grid minor;
% legend('equation Fram 1987','equation Ganter 2006')
title('White matter')
xlabel('MTV'); ylabel('Number of voxels');
set(gca,'FontSize',35);
xlim([0.15 0.5])

subplot(1,2,2)
histogram(MTV_fram_nii.img(logical(csf_mask_nii.img)),60);
hold on;
histogram(MTV_ganter_nii.img(logical(csf_mask_nii.img)),60);

y_lim = get(gca,'ylim');
plot([hist_peak_MTV_Fram_CSF hist_peak_MTV_Fram_CSF],y_lim, 'Color', 'b', 'LineWidth',3);
plot([hist_peak_MTV_Ganter_CSF hist_peak_MTV_Ganter_CSF],y_lim, 'Color', 'r', 'LineWidth',3);

hold off;
grid on; grid minor;
% legend('equation Fram 1987','equation Ganter 2006')
title('CSF')
xlabel('MTV'); ylabel('Number of voxels');
set(gca,'FontSize',35);
xlim([-0.4 0.7])


saveas(f,'/Users/slevy_local/Dropbox/simon_levy/master/master_thesis/fig/graph_soutenance/comparison_Fram_Ganter_MTV_WM_CSF_d_sp_pain_01.jpg')






