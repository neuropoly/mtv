function [ diff_ratio_actual_theo ] = diff_signal_ratios_actual_theo( T1, T2, fa1, fa2, TR, phi, ratio_actual )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% compute lambda with both flip angles
lbda1=lambda_contfract(TR,fa1,T1,T2,phi);
lbda2=lambda_contfract(TR,fa2,T1,T2,phi);
% compute conjugates
lbda1_conj=conj(lbda1);
lbda2_conj=conj(lbda2);

% calculate the ratio of the signals of both images (with different flip
% angles)
E1=exp(-TR/T1);
D1=1-E1*cos(fa1)-0.5*(1-cos(fa1))*(1+E1)*(lbda1+lbda1_conj)+(E1-cos(fa1))*(abs(lbda1)^2);
D2=1-E1*cos(fa2)-0.5*(1-cos(fa2))*(1+E1)*(lbda2+lbda2_conj)+(E1-cos(fa2))*(abs(lbda2)^2);

ratio_theo = abs(sin(fa1)*D2*(1-lbda1_conj))/abs(sin(fa2)*D1*(1-lbda2_conj));

diff_ratio_actual_theo=abs(ratio_actual-ratio_theo);

end

