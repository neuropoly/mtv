function [ lambda ] = lambda_contfract( TR, fa, T1, T2, phi )
%UNTITLED3 Summary of this function goes here
%   fa: flip angle (in degree)

nb_iter_max=10000;

y=exp(1i*phi);
E1=exp(-TR/T1);
E2=exp(-TR/T2);

% coefficients of the continued fraction expansion
f=zeros(1,nb_iter_max); % vector of the continued fraction with the different iterations
a=zeros(1,nb_iter_max);
b=zeros(1,nb_iter_max);
alpha=zeros(1,nb_iter_max);
beta=zeros(1,nb_iter_max);
gamma=zeros(1,nb_iter_max);
C=zeros(1,nb_iter_max);
D=zeros(1,nb_iter_max);
delta=zeros(1, nb_iter_max);

% intialization - !!! index 1 in Matlab corresponds to index 0 of the
% coefficients in usual notations (cannot avoid this because Matlab indexes start at 1)!!!
alpha(2) = 1-y*E1*cos(fa);
beta(2) = cos(fa)-y*E1;
gamma(2) = 0.5*(alpha(2)-beta(2));
b(1) = -(beta(2)/gamma(2))*(E2^2)*y;
a(2) = (gamma(2)/alpha(2)+beta(2)/gamma(2))*(E2^2)*y;

% 
tiny=10^(-30);
eps=10^(-15);
if b(1)==0
    f(1)=tiny;
else
    f(1)=b(1);
end
C(1) = f(1);
D(1)=0;
jj=2;
while abs(delta(jj-1)-1)>=eps
    % calculate alpha, beta and gamma for order jj+1 (because needed to
    % calculate b(jj))
    alpha(jj+1) = 1-(y^jj)*E1*cos(fa);
    beta(jj+1) = cos(fa)-(y^jj)*E1;
    gamma(jj+1) = 0.5*(alpha(jj+1)-beta(jj+1));
    % calculate b(jj)
    b(jj)=1+((gamma(jj)*beta(jj+1))/(alpha(jj)*gamma(jj+1)))*(E2^2)*(y^(2*(jj-1)+1));
    a(jj+1)=-(gamma(jj)/alpha(jj))*(gamma(jj+1)/alpha(jj+1)+beta(jj+1)/gamma(jj+1))*(E2^2)*(y^(2*jj-1));
    % calculate D(jj) and C(jj)
    D(jj)=b(jj)+a(jj)*D(jj-1);
    if D(jj)==0
        D(jj)=tiny;
    end
    C(jj)=b(jj)+a(jj)/C(jj-1);
    if C(jj)==0
        C(jj)=tiny;
    end
    D(jj)=1/D(jj);
    % calculate delta(jj)
    delta(jj)=C(jj)*D(jj);
    f(jj)=f(jj-1)*delta(jj);
    
    % increment
    jj=jj+1;
    
end
    
lambda=f(jj-1);  

end

