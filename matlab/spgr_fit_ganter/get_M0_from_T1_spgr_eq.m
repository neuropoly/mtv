function [ M0 ] = get_M0_from_T1_spgr_eq( S, T1, T2, fa, TR, phi, TE, T2star )
%UNTITLED2 Summary of this function goes here
%   fa: nominal flip angle (in radians)
%   phi: RF phase difference increment (in radians)
%   TE: Echo Time (in s)
%   T2star: guess of the T2* (in s)

unit_signal_theo=signal_ss_rf_spoiled_gre( T1, T2, fa, TR, phi );

M0=S/(unit_signal_theo*exp(-TE/T2star));

end

