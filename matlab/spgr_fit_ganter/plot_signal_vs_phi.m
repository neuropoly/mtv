%% Parameters
fa=30;
T1=1;
T2=T1;
TR=0.05;

%% Plot signal as a function of phi (RF phase difference increment)
fh=@(phi) signal_ss_rf_spoiled_gre(T1, T2, fa, TR, phi);
fh_ideal_spoiling=@(phi)(1-exp(-TR/T1))*sind(fa)/(1-exp(-TR/T1)*cosd(fa));
figure
hold on
fplot(fh,[0 180])
fplot(fh_ideal_spoiling,[0 180])
hold off
xlabel('\Phi (in degrees)');
ylabel('|M_x_y|');
legend('Steady-state signal','Ideal spoiling');
set(gca,'FontSize',20);