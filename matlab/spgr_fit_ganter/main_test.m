%% Parameters of the FLASH acquisitions
TR=0.035; % Repetition Time (in s)
fa1=1.12*4; % flip angle 1 (in degrees)
fa2=1.12*20; % flip angle 2 (in degrees)
phi=50; % linear phase increment used for spoiling (in degrees)
T2=0.070; % guess of the transversal relaxation time in the tissue (in s)

%% Data
% flash1_nii=load('/Volumes/slevy-4/data/criugm/d_sp_pain_pilot8/mtv/rescan/spgr5crop.nii.gz');
% flash2_nii=load('/Volumes/slevy-4/data/criugm/d_sp_pain_pilot8/mtv/rescan/spgr20crop.nii.gz');
% seg_nii=load('/Volumes/slevy-4/data/criugm/d_sp_pain_pilot8/mtv/rescan/seg.nii.gz');
% mean_std_median_SbS1=compute_metric_mean_std_median_SbS(flash1_nii.img,seg_nii.img);
S1=200;
S2=300;
ratio_actual=S1/S2;

%% Find T1
% convert angles to radians
fa1=degtorad(fa1);fa2=degtorad(fa2);phi=degtorad(phi);
% solve equation numerically
[T1,fval,exitflag] = fminsearch(@(T1) diff_signal_ratios_actual_theo(T1, T2, fa1, fa2, TR, phi, ratio_actual),0.3,optimset('MaxFunEvals',100000000000000000,'TolFun',10^(-20),'TolX',10^(-10)));

