%% simulation size and mask
phantom2=repmat(phantom('Modified Shepp-Logan',50),[1 1 3])+1e-15;
dims=size(phantom2); mask = ~~sct_create_mask_bg(phantom2,0.1,1); 
watermask = phantom2>0.9;
mask_corT1 = 0;
imagesc3D(mask)
%mask = ones(dims); mask=~~mask;
dims(4) = 4; % nb of coils
dims(dims==0)=1;
spacing = 1./size(rhok); spacing = spacing(1:3); spacing = spacing/min(spacing);
%% data size and mask
slice=5:9;
rhok = double(load_nii_data(['coil1.nii.gz'],slice));
for ik=2:3
    rhok(:,:,:,ik) = double(load_nii_data(['coil' num2str(ik) '.nii.gz'],slice));
end
mg=50; rhok = rhok(round(end/2-mg):round(end/2+mg),round(end/2-mg):round(end/2+mg),:,:);

sos=mean(rhok,4);
imagesc3D(rhok)

dims=size(rhok);
% create mask
mask = ~~sct_create_mask_bg(sos,0.001); 
imagesc3D(mask);
watermask=~~load_nii_data('CSF_seg.nii',slice);
T1 = load_nii_data('T1.nii',slice);
mask = mask | watermask;

%% data size and mask

rhok=double(load_nii_data('M0s.nii.gz'));
sos=mean(rhok,4);
mask = ~~load_nii_data('SPGRs_ref_seg.nii.gz');

for level=1:size(mask,3);
    se = strel('disk',2);
    mask(:,:,level)=imdilate(mask(:,:,level),se);
end

watermask = ~~load_nii_data('CSF_seg.nii');
mask_corT1 = mask;
mask = mask | watermask;
T1 = load_nii_data('T1.nii.gz');
hdr = load_nii('T1.nii.gz');
spacing = hdr.hdr.dime.pixdim(2:4);
%% create polynomial basis
degree=5;
[Phi, str ]= constructpolynomialmatrix3d(dims(1:3),find(mask),degree);
nbasis=size(Phi,2);
% [phi10,phi01] = meshgrid(1:dims(1),1:dims(2));
% % imagesc(phi10)
% % imagesc(phi01)
% phi10 = phi10(mask); phi10=phi10(:); phi01=phi01(mask); phi01=phi01(:);
% 
% R=3;
% Phi = zeros(length(find(mask)),R^2);
% Porder=zeros(2*R,1);
% for q=0:R-1
%     for r=0:R-1
%         ind = sub2ind([R R],q+1,r+1);
%         Porder(ind)=q+r;
%         Phi(:,ind) = phi10.^q.*phi01.^r;
%     end
% end

%% Visualize
sk = zeros(dims(1:3));
for ip=1:size(Phi,2);
    sk(mask)=Phi(:,ip);
    imagesc3D(sk); drawnow; pause
end

%% Simulation
rhok = zeros(dims);
coef = zeros(size(Phi,2),dims(4));
for ik=1:dims(4)
    sk = zeros(dims(1:3));
    coef(:,ik)=5*randn(size(Phi,2),1);
    sk(mask)=Phi*coef(:,ik);
    rhok(:,:,:,ik)=phantom2.*(sk - min(sk(:))+3) + 10*rand(dims(1:3));
end
imagesc3D(rhok)

%% initialize data
f0= rhok(:,:,:,1);% load_nii_data('F0_init.nii.gz');% %ones(dims(1:3)); % initialize with sos
f0 = f0./mean(f0(watermask)); f0(watermask)=1; f0(~mask)=0;

clear basis; basis{1}=Phi;
coef = zeros(size(Phi,2),dims(4));
for ik=1:dims(4)
    coef(:,ik) = fit3dpolynomialmodel(rhok(:,:,:,ik)./f0,mask,degree,basis);
end

%% Solve
rhok_m = reshape2D(rhok,4)'; rhok_m = rhok_m(mask,:);

f0init=f0; %+0.01*randn(size(f0));
f02=f0init;
dinit=coef(:); %+5*randn(size(rhok_m,2)*size(Phi,2),1);
d2=dinit;
index_notwater=find(mask); index_notwater = ismember(index_notwater,find(~watermask));

sk = zeros(dims(1:3));
dPhi = zeros(dims(4),size(index_notwater,1));
weight = double(mask); weight(mask_corT1)= weight(mask_corT1)*1; weight(watermask)=weight(watermask)*1;
for ii=1:1000
    if mod(ii,1)==0, imagesc3D(f02,[0 1.1]); drawnow; colorbar; end
    if length(find(mask_corT1))
        f02(mask_corT1 & mask) = 1./polyval(polyfit(1./T1(mask_corT1 & mask & T1>0.1),1./f02(mask_corT1 & mask & T1>0.1),1),1./T1(mask_corT1 & mask),1);
    end
    
    %coefinit=randn(size(Phi,2),size(rhok_m,2));
    for ik =1:size(rhok_m,2)
        tmm = mtv_fit3dsplinemodel(rhok(:,:,:,ik)./f02,mask,weight,0.8,spacing);
        Poly(:,:,:,ik) = tmm; tmm(~mask)=nan;
        imagesc3D(tmm); drawnow;
        dPhi(ik,:) = tmm(mask);
    end
    
%     fPhi = repmat(f02(mask),[1 size(Phi,2)]).*Phi;
     rhok2 = diag(rhok_m'*rhok_m);
%     fPhi2=fPhi'*fPhi;
%     rhofPhi=rhok_m'*fPhi;

%     options = optimoptions(@fminunc,'GradObj','on','MaxIter',30,'Display','off');
%     d2 = fminunc(@(d) objfun_d(rhok2,fPhi2,rhofPhi, d),d2(:),options);
%         
%     d2 = reshape(d2,[size(Phi,2) size(rhok_m,2)]);
%     dPhi= d2'*Phi';
    rhodPhi = rhok_m'.*dPhi;
    
    
    options = optimoptions(@fmincon,'GradObj','on','MaxIter',5,'Algorithm','interior-point','Hessian','lbfgs','Display','iter');    
    f02(mask & ~watermask) = fmincon(@(f0m) objfun_f(rhok2,dPhi(:,index_notwater),rhodPhi(:,index_notwater),f0m),f02(mask & ~watermask), [], [], [], [], zeros(length(find(mask & ~watermask)),1),ones(length(find(mask & ~watermask)),1),[], options);
    
%     sk(mask) = dPhi(1,:);
%     tmp = rhok(:,:,:,1)./sk;
%     f02(mask_corT1 & mask) = tmp(mask_corT1 & mask);
end

%%
figure(4)
for ik=1:size(rhok_m,2),
    subplot(size(rhok_m,2),4,(ik-1)*4 +1)
    imagesc3D(rhok(:,:,:,ik),[0 10000]);
    
    subplot(size(rhok_m,2),4,(ik-1)*4 +2)
    sk = zeros(dims(1:3));
    sk(mask) = Phi*dinit(size(Phi,2)*(ik-1)+1:size(Phi,2)*ik);
    imagesc3D(sk);
    
    subplot(size(rhok_m,2),4,(ik-1)*4 +3)
    sk = Poly(:,:,:,ik);
    imagesc3D(sk, [0 4000]);
    
    subplot(size(rhok_m,2),4,(ik-1)*4 +4)
    %tmp = f0.*sk;
    tmp = rhok(:,:,:,ik)./sk;
    imagesc3D(tmp,[0 1.1]);
    
end

%
%     subplot(2,3,1)
%     imagesc3D(rhok(:,:,:,1));
%     subplot(2,3,2)
%     vr=rhok(:,:,:,1)./s1; vr(isinf(vr))=nan;
%     imagesc3D(vr);
%     subplot(2,3,3)
%     imagesc3D(s1);
%     subplot(2,3,4)
%     imagesc3D(rhok(:,:,:,2));
%     subplot(2,3,5)
%     imagesc3D(rhok(:,:,:,2)./s2./(rhok(:,:,:,1)./s1));
%     subplot(2,3,6)
%     imagesc3D(s2);
