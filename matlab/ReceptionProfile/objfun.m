function [s, gd]=objfun(A2,d, Phi,mask,dims)
% k=zeros(dims);
% k(mask)=Phi*d(1:size(Phi,2));
% imagesc3D(k); drawnow
lambda = 0e9; %beta=1e-2;
reg = d'*d;
s = d'*A2*d + lambda*((lambda+reg)/reg)-1;
gd=2*(A2*d);
