function [s, gd]=objfun_d_f(rhok2,dPhi2,rhodPhi,fPhi2,rhofPhi, fd)
% k=zeros(dims);
% k(mask)=Phi*d(1:size(Phi,2));
% imagesc3D(k); drawnow
s = 0; gd = 0; gf=0;
for ik=1:size(rhofPhi,1)
    dk=fd(size(fPhi2,1)*(ik-1)+1:size(fPhi2,1)*ik);
    s = s + rhok2(ik) + dk'*fPhi2*dk - rhofPhi(ik,:)*dk - dk'*rhofPhi(ik,:)';
    gd(size(fPhi2,1)*(ik-1)+1:size(fPhi2,1)*ik,1) = - 2*rhofPhi(ik,:)' + 2*fPhi2*dk ;
    gf = gf + - 2*rhodPhi(ik,:)' + 2*dPhi2(ik,:)'.*fd(size(fPhi2,1)*size(rhofPhi,1)+1:end) ;
end

gd = [gd;gf];

%disp(s)
