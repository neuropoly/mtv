%% simulation size and mask
phantom2=repmat(phantom('Modified Shepp-Logan',80),[1 1 3]);
dims=size(phantom2); mask = ~~sct_create_mask_bg(phantom2,0.1,1); 
imagesc3D(mask)
%mask = ones(dims); mask=~~mask;
dims(4) = 2; % nb of coils
dims(dims==0)=1;
%% data size and mask
slice=5:9;
rhok = double(load_nii_data(['coil1.nii.gz'],slice));
for ik=2:3
    rhok(:,:,:,ik) = double(load_nii_data(['coil' num2str(ik) '.nii.gz'],slice));
end
%mg=50; rhok = rhok(round(end/2-mg):round(end/2+mg),round(end/2-mg):round(end/2+mg),:,:);

sos=mean(rhok,4);
imagesc3D(rhok)

dims=size(rhok);
% create mask
mask = ~~sct_create_mask_bg(sos,0.1); 
imagesc3D(mask);

%% create polynomial basis
degree=3;
[Phi, str ]= constructpolynomialmatrix3d(dims(1:3),find(mask),degree);
nbasis=size(Phi,2);

%% Visualize
sk = zeros(dims(1:3));
for ip=1:size(Phi,2);
    sk(mask)=Phi(:,ip);
    imagesc3D(sk); drawnow; pause
end

%% Simulation
rhok = zeros(dims);
coef = zeros(size(Phi,2),dims(4));
for ik=1:dims(4)
    sk = zeros(dims(1:3));
    coef(:,ik)=5*randn(size(Phi,2),1);
    sk(mask)=Phi*coef(:,ik);
    rhok(:,:,:,ik)=phantom2.*(sk - min(sk(:))+3) + 5*rand(dims(1:3));
end
imagesc3D(rhok)

%% initialize data
f0=mean(rhok,4); %ones(dims(1:3)); % initialize with sos

basis{1}=Phi;
coef = zeros(size(Phi,2),dims(4));
for ik=1:dims(4)
    coef(:,ik) = fit3dpolynomialmodel(rhok(:,:,:,ik)./f0,mask,degree,basis);
end

%% Solve
rhok_m = reshape2D(rhok,4)'; rhok_m = rhok_m(mask,:);

f0init=f0; %+0.01*randn(size(f0));
f02=f0init;
dinit=coef(:)+5*randn(size(rhok_m,2)*size(Phi,2),1);
d2=dinit;

fPhi = repmat(f02(mask),[1 size(Phi,2)]).*Phi;
rhok2 = diag(rhok_m'*rhok_m);
fPhi2=fPhi'*fPhi;
rhofPhi=rhok_m'*fPhi;


d2 = reshape(d2,[size(Phi,2) size(rhok_m,2)]);
dPhi= d2'*Phi';
dPhi2 = dPhi.^2;
rhodPhi = rhok_m'.*dPhi;


options = optimoptions(@fmincon,'GradObj','on','Algorithm','interior-point','Hessian','lbfgs','Display','iter','MaxIter',100);
fd = fmincon(@(fd) objfun_d_f(rhok2,dPhi2,rhodPhi,fPhi2,rhofPhi, fd),[d2(:); f02(mask)], [], [], [], [],[-inf(size(Phi,2)*size(rhok_m,2),1); zeros(length(find(mask)),1)],[],[], options);
d2 = fd(1:size(Phi,2)*size(rhok_m,2)); f02(mask) = fd(size(Phi,2)*size(rhok_m,2)+1:end);
imagesc3D(f02);


figure(4)
for ik=1:size(rhok_m,2),
    subplot(size(rhok_m,2),4,(ik-1)*4 +1)
    imagesc3D(rhok(:,:,:,ik));
    
    subplot(size(rhok_m,2),4,(ik-1)*4 +2)
    sk = zeros(dims(1:3));
    sk(mask) = Phi*dinit(size(Phi,2)*(ik-1)+1:size(Phi,2)*ik);
    imagesc3D(sk);
    
    subplot(size(rhok_m,2),4,(ik-1)*4 +3)
    sk = zeros(dims(1:3));
    sk(mask) = Phi*d2(size(Phi,2)*(ik-1)+1:size(Phi,2)*ik)';
    imagesc3D(sk);
    
    subplot(size(rhok_m,2),4,(ik-1)*4 +4)
    %tmp = f0.*sk;
    tmp = rhok(:,:,:,ik)./sk;
    imagesc3D(tmp,[max(0,prctile(tmp(mask),1)) prctile(tmp(mask),99)]);
    
end

%
%     subplot(2,3,1)
%     imagesc3D(rhok(:,:,:,1));
%     subplot(2,3,2)
%     vr=rhok(:,:,:,1)./s1; vr(isinf(vr))=nan;
%     imagesc3D(vr);
%     subplot(2,3,3)
%     imagesc3D(s1);
%     subplot(2,3,4)
%     imagesc3D(rhok(:,:,:,2));
%     subplot(2,3,5)
%     imagesc3D(rhok(:,:,:,2)./s2./(rhok(:,:,:,1)./s1));
%     subplot(2,3,6)
%     imagesc3D(s2);
