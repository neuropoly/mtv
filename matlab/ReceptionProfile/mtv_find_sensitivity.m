%% simulation size and mask
phantom=phantom3d; phantom=phantom(:,:,55:58);
dims=size(phantom); mask = ~~sct_create_mask_bg(phantom,0.1); 

%mask = ones(dims); mask=~~mask;
dims(4) = 4; % nb of coils
dims(dims==0)=1;
%% data size and mask
slice=5:9;
rhok = double(load_nii_data(['coil1.nii.gz'],slice));
for ik=2:3
    rhok(:,:,:,ik) = double(load_nii_data(['coil' num2str(ik) '.nii.gz'],slice));
end
%mg=50; rhok = rhok(round(end/2-mg):round(end/2+mg),round(end/2-mg):round(end/2+mg),:,:);

sos=mean(rhok,4);
imagesc3D(rhok)

dims=size(rhok);
% create mask
mask = ~~sct_create_mask_bg(sos,0.1); 
imagesc3D(mask);

%% create polynomial basis
degree=3;
[Phi, str ]= constructpolynomialmatrix3d(dims(1:3),find(mask),degree);
nbasis=size(Phi,2);
% [phi10,phi01] = meshgrid(1:dims(1),1:dims(2));
% % imagesc(phi10)
% % imagesc(phi01)
% phi10 = phi10(mask); phi10=phi10(:); phi01=phi01(mask); phi01=phi01(:);
% 
% R=3;
% Phi = zeros(length(find(mask)),R^2);
% Porder=zeros(2*R,1);
% for q=0:R-1
%     for r=0:R-1
%         ind = sub2ind([R R],q+1,r+1);
%         Porder(ind)=q+r;
%         Phi(:,ind) = phi10.^q.*phi01.^r;
%     end
% end

%% Visualize
sk = zeros(dims(1:3));
for ip=1:size(Phi,2);
    sk(mask)=Phi(:,ip);
    imagesc3D(sk); drawnow; pause(0.5)
end

%% Simulation
rhok = zeros(dims);
coef = zeros(size(Phi,2),dims(4));
for ik=1:dims(4)
    sk = zeros(dims(1:3));
    coef(:,ik)=100*randn(size(Phi,2),1);
    sk(mask)=Phi*coef(:,ik);
    rhok(:,:,:,ik)=phantom.*(sk - min(sk(:))+50) + 20*rand(dims(1:3));
end
imagesc3D(rhok)

%% initialize data

basis{1}=Phi;
coef = zeros(size(Phi,2),dims(4));
for ik=1:dims(4)
    coef(:,ik) = fit3dpolynomialmodel(rhok(:,:,:,ik),mask,degree,basis); 
end

%% Solve
rhok_m = reshape2D(rhok,4)';
PHIk = zeros(length(find(mask)),size(Phi,2));
for ik=1:size(rhok_m,2)
    PHIk(:,:,ik)=repmat(rhok_m(mask,ik),[1,size(Phi,2)]).*Phi;
    A = [repmat(rhok_m(mask,1),[1,size(Phi,2)]).*Phi, -repmat(rhok_m(mask,2),[1,size(Phi,2)]).*Phi];
end

PHIkPHIk = zeros(size(Phi,2),size(Phi,2),size(rhok_m,2));
for ik=1:size(rhok_m,2)
    PHIkPHIk(:,:,ik) = PHIk(:,:,ik)'*PHIk(:,:,ik);
end

A2ij = cell(size(rhok_m,2),size(rhok_m,2));
for iki=1:size(rhok_m,2)
    for ikj=1:size(rhok_m,2)
        if iki==ikj
            A2ij{iki,ikj} = sum(PHIkPHIk(:,:,setdiff(1:size(rhok_m,2),iki)),3);
        else
            A2ij{iki,ikj} = - PHIk(:,:,iki)'*PHIk(:,:,ikj);
        end
    end
end
A2 = cell2mat(A2ij);
%A2 = A'*A; 
%coef=randn(size(Phi,2),size(rhok_m,2));
options = optimoptions(@fminunc,'GradObj','on','MaxIter',200);
d = fminunc(@(d) objfun(A2,d,Phi,mask,dims(1:3)),coef(:)+0.0*randn(size(rhok_m,2)*size(Phi,2),1),options);
% [V,D] = eig(A2); [~,I]=min(diag(D));
% d = V(:,I);
% d = null(A);

% for id=1:size(d,2)
%     s2 = zeros(dims(1:3));
%     s2(mask) = Phi*d(1:end/2,id);
%     imagesc3D(s2); drawnow; pause(0.5)
% end
% s1 = zeros(dims(1:3));
% s1(mask) = Phi*d(end/2+1:end,1);
%imagesc(rho2./s1./mean(rho2(mask)./s1(mask)),[-1 1]);

figure(4)
for ik=1:size(rhok_m,2),
    subplot(size(rhok_m,2),4,(ik-1)*4 +1)
    imagesc3D(rhok(:,:,:,ik));
    
    subplot(size(rhok_m,2),4,(ik-1)*4 +2)
    sk = zeros(dims(1:3));
    sk(mask) = Phi*coef(:,ik);
    imagesc3D(sk);
    
    subplot(size(rhok_m,2),4,(ik-1)*4 +3)
    sk = zeros(dims(1:3));
    sk(mask) = Phi*d(size(Phi,2)*(ik-1)+1:size(Phi,2)*ik,1);
    imagesc3D(sk);
    
    subplot(size(rhok_m,2),4,(ik-1)*4 +4)
    tmp = rhok(:,:,:,ik)./sk;
    imagesc3D(tmp,[prctile(tmp(mask),1) prctile(tmp(mask),99)]);
    
end

%
%     subplot(2,3,1)
%     imagesc3D(rhok(:,:,:,1));
%     subplot(2,3,2)
%     vr=rhok(:,:,:,1)./s1; vr(isinf(vr))=nan;
%     imagesc3D(vr);
%     subplot(2,3,3)
%     imagesc3D(s1);
%     subplot(2,3,4)
%     imagesc3D(rhok(:,:,:,2));
%     subplot(2,3,5)
%     imagesc3D(rhok(:,:,:,2)./s2./(rhok(:,:,:,1)./s1));
%     subplot(2,3,6)
%     imagesc3D(s2);
