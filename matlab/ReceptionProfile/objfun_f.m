function [s, gd]=objfun_f(rhok2,dPhi,rhodPhi, f0m)
% k=zeros(dims);
% k(mask)=Phi*d(1:size(Phi,2));
% imagesc3D(k); drawnow
dPhi2 = dPhi.^2;
s = 0; gd = zeros(size(rhodPhi,2),1); 
% mask f0 and gd
%weights = ;
mu=0e7;
for ik=1:size(rhodPhi,1)
    %r = corrcoef(dPhi(ik,:)',f0m);
    s = s + rhok2(ik) + f0m'.*dPhi2(ik,:)*f0m - rhodPhi(ik,:)*f0m - f0m'*rhodPhi(ik,:)';% + mu*r(2,1)^2;
    gd = gd - 2*rhodPhi(ik,:)' + 2*dPhi2(ik,:)'.*f0m;% + 2*mu*dPhi(ik,:)*f0m;
end
%disp(s)
% weights = (1./rhok2).^0.5;
% s = s*weights;
% gd = gd*weights;