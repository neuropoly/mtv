function [  ] = correct_receive_profile_N4ITK( m0_subjspace_fname, warp_mtv2template, warp_template2mtv, csf_mask_subj_space_fname, out_WF_fname, out_MTV_fname, out_M02template_fname, out_M0corrected2template_fname, out_MTV2template_fname, out_bias2template_fname )
%This function estimates the coils reception profile using the algorithm
%"N4ITK" from Tustison 2010 IEEE Vol.29 No. 6.
%   m0_subjspace_fname, warp_mtv2template, warp_template2mtv, csf_mask_subj_space_fname, out_WF_fname, out_MTV_fname, out_M02template_fname, out_M0corrected2template_fname, out_MTV2template_fname, out_bias2template_fname

% default
[fcn_path,~,~] = fileparts(mfilename('fullpath'));
mask_csf_wm_gm_fname=fullfile(fcn_path,'MNI-Poly-AMU_WMtract__sum00_31.nii.gz');

%% Preprocessing
% remove negative value in M0 (set to min > 0)
m0=load_nii_data(m0_subjspace_fname);
m0(m0<=0)=min(m0(m0>0));
[m0_path,~,~] = fileparts(m0_subjspace_fname);
m0_subjspace_thr_space=fullfile(m0_path,'M0_subj_space_thr.nii.gz');
save_nii_v2(m0,m0_subjspace_thr_space,m0_subjspace_fname,64);

% register M0 to template
unix(['source sct_env; sct_apply_transfo -i ' m0_subjspace_thr_space ' -d $SCT_DIR/data/template/MNI-Poly-AMU_WM.nii.gz -w ' warp_mtv2template ' -o ' out_M02template_fname]);

%% Estimate bias
% find min and max z to threshold the mask
m0_template=load_nii_data(out_M02template_fname);
[~,~,zmin]=ind2sub(size(m0_template),find(m0_template,1,'first'));
[~,~,zmax]=ind2sub(size(m0_template),find(m0_template,1,'last'));
mask=load_nii_data(mask_csf_wm_gm_fname);
for z=1:size(mask,3)
    if z<zmin+10 || z> zmax-10
        mask(:,:,z)=0;
    end
end
[m0_template_path,~,~] = fileparts(out_M02template_fname);
mask_thr_fname=fullfile(m0_template_path,'mask_CSF_WM_GM.nii.gz');
save_nii_v2(mask,mask_thr_fname,mask_csf_wm_gm_fname);
% run correction
unix(['N4BiasFieldCorrection -i ' out_M02template_fname ' -o [' out_M0corrected2template_fname ',' out_bias2template_fname '] -d 3 -r 1 -b [200,4] -w ' mask_thr_fname ' -s 2 -c [50x50x50x50,0.0]']);

% % rescale the estimated bias field between 0 and 2 (to avoid introducing a
% % subject-related bias)
% B=load_nii_data('./b1minus2template.nii.gz');B=double(B);
% old_min=min(min(min(B)));
% old_max=max(max(max(B)));
% new_min=0;
% new_max=2;
% B_rescaled = ((B - old_min) * (new_max - new_min)) / (old_max - old_min);
% save_nii_v2(B_rescaled,out_bias2template_fname,'./b1minus2template.nii.gz',64)
% 
% % correct M0
% unix(['sct_maths -i ' out_M02template_fname ' -div ' out_bias2template_fname ' -o ' out_M0corrected2template_fname]);
% % delete('./b1minus2template.nii.gz');

% register M0corrected to subject space
m0corrected_subj_space_fname=fullfile(m0_path,'M0_corrected.nii.gz');
unix(['sct_apply_transfo -i ' out_M0corrected2template_fname ' -d ' m0_subjspace_fname ' -w ' warp_template2mtv ' -o ' m0corrected_subj_space_fname]);

%% normalize by CSF
CSF = load_nii_data(csf_mask_subj_space_fname);
m0=load_nii_data(m0corrected_subj_space_fname);m0=double(m0);

% calculate the M0 value within CSF
[csfValues, csfDensity]= ksdensity(m0(find(CSF)), min(m0(find(CSF))):0.001:max(m0(find(CSF))) );
CalibrationVal= csfDensity(find(csfValues==max(csfValues)));% median(PD(find(CSF)));

% normalize
WF=m0./CalibrationVal(1);

% remove outlayers
WF(WF<0)=0;
WF(WF>2)=2;

% compute MTV
MTV = ones(size(WF))-WF;

% save the WF and the MTV maps
save_nii_v2(single(WF),out_WF_fname,csf_mask_subj_space_fname,64);
save_nii_v2(MTV, out_MTV_fname, csf_mask_subj_space_fname, 64);

% register MTV to template
unix(['source sct_env; sct_apply_transfo -i ' out_MTV_fname ' -d $SCT_DIR/data/template/MNI-Poly-AMU_WM.nii.gz -w ' warp_mtv2template ' -o ' out_MTV2template_fname]);

end

