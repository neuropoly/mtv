%% Improve reception profile (RP) correction

cd('/Volumes/slevy/data/criugm/d_sp_pain_30_retest/mtv')
csffile='spgr20crop_csf.nii.gz';
segfile='spgr20crop_seg_ok.nii.gz';
PDfile='M0.nii.gz';
outputWFfile='wf_uncorrected_RP.nii.gz';
outputMTVfile='mtv_uncorrected_RP.nii.gz';

% -------------------------------------------------------------------------
%% Normalize by CSF
CSF = load_nii_data(csffile);CSF=double(CSF);
seg = load_nii_data(segfile);seg=double(seg);
PD=load_nii_data(PDfile);PD=double(PD);

%% calcute the CSF PD

% % find the white matter mean pd value from segmetation.
% wmV=mean(PD(seg==2 & PD>0));
% 
% % assure that the CSF ROI have pd value that are resnable.  The csf roi is a reslut of segmentation algoritim runed on the
% % T1wighted image and cross section with T1 values. Yet  the ROI may have some contaminations or segmentation faules .
% %Therefore, we create some low and up bonderies. No CSF with PD values that are the white matter PD value(too low) or double the white matter values (too high).
% CSF1=CSF & PD>wmV & PD< wmV*2;

%To calibrate the PD we find the scaler that shift the csf ROI to be eqal to 1. --> PD(CSF)=1;
% To find the scale we look at the histogram of PD value in the CSF. Since it's not trivial to find the peak we compute the kernel density (or
% distribution estimates). for detail see ksdensity.m
%The Calibrain vhistogram of the PD values in the let find the scalre from the maxsimum of the csf values histogram
[csfValues, csfDensity]= ksdensity(PD(find(CSF)), min(PD(find(CSF))):0.001:max(PD(find(CSF))) );
CalibrationVal= csfDensity(find(csfValues==max(csfValues)));% median(PD(find(CSF)));

%% calibrate the pd by the pd of the csf roi
WF=PD./CalibrationVal(1);

% let cut outlayers
WF(WF<0)=0;
WF(WF>2)=2;

% save the WM map
save_nii_v2(single(WF),outputWFfile,csffile,64);

%% Compute MTV from the WF_map
WF_map = load_nii_data(outputWFfile);
MTV_map = ones(size(WF_map))-WF_map;
save_nii_v2(MTV_map, outputMTVfile, csffile, 64);
% -------------------------------------------------------------------------

%% Register the uncorrected map to the template
unix('source sct_env; sct_apply_transfo -i mtv_uncorrected_RP.nii.gz -d $SCT_DIR/data/template/MNI-Poly-AMU_WM.nii.gz -w ../warping_fields/warp_mtv2template.nii.gz -o ../template/mtv_uncorrected_RP2template.nii.gz');

%% New method: FUCCY C-MEANS

% Example
% Compile the c-code
mex BCFCM3D.c -v
% Load test volume
load MRI;
% Convert to single
D = single(squeeze(D))/88;
% Class prototypes (means)
v = [0 0.34 0.72 0.75 1.4];
% Do the fuzzy clustering
[B,U]=BCFCM3D(D,v,struct('maxit',5,'epsilon',1e-5,'sigma',1));
% Show results
figure, 
subplot(2,2,1), imshow(D(:,:,15)), title('A slice of input volume');
subplot(2,2,2), imshow(squeeze(U(:,:,15,2:4))), title('3 classes of the partition matrix');
subplot(2,2,3), imshow(B(:,:,15),[]), title('Estimated biasfield');
subplot(2,2,4), imshow(D(:,:,15)-B(:,:,15)), title('Corrected slice');

% Test
% crop image
xmin='36';
xmax='87';
ymin='35';
ymax='79';
zmin='331';
zmax='470';
unix(['sct_crop_image -i mtv_uncorrected_RP2template.nii.gz -o mtv_uncorrected_RP2template_crop.nii.gz -start ',xmin,',',ymin,',',zmin,' -end ',xmax,',',ymax,',',zmax,' -dim 0,1,2']);

mtv_uncorrected=load_nii_data('mtv_uncorrected_RP2template.nii.gz');
v = [0 0.27 0.34 0.8];
mtv_uncorrected(mtv_uncorrected<0)=0;
mtv_uncorrected(mtv_uncorrected>1)=1;
[B,U]=BCFCM3D(single(mtv_uncorrected),v,struct('maxit',10,'epsilon',0.001,'sigma',[8 8 8],'alpha',0.7,'p',2));
% rescale
% rmin_old=min(min(min(B)));
% rmax_old=max(max(max(B)));
% rmin=0; rmax=255;
% B_rescaled = ((B - rmin_old) * (rmax - rmin)) / (rmax_old - rmin_old);
save_nii_v2(B,'bias_field.nii.gz','mtv_uncorrected_RP2template.nii.gz',64);
save_nii(make_nii(U),'partition_matrix.nii.gz');
mtv_corrected=double(mtv_uncorrected)./exp(double(B));
save_nii_v2(mtv_corrected,'mtv_corrected.nii.gz','mtv_uncorrected_RP2template.nii.gz',64);

%% New method: N4BiasFieldCorrection

t1_fname='T12template.nii.gz';
M0_fname='M02template.nii.gz';
cord_fname='MNI-Poly-AMU_cord.nii.gz';
wm_fname='MNI-Poly-AMU_WM.nii.gz';
gm_fname='MNI-Poly-AMU_GM.nii.gz';
csf_fname='WMtract__31.nii.gz';
xmin='36';
xmax='87';
ymin='35';
ymax='79';
zmin='331';
zmax='470';
unix(['sct_crop_image -i ',t1_fname,' -o T12template_crop.nii.gz -start ',xmin,',',ymin,',',zmin,' -end ',xmax,',',ymax,',',zmax,' -dim 0,1,2']);
unix(['sct_crop_image -i ',cord_fname,' -o cord_crop.nii.gz -start ',xmin,',',ymin,',',zmin,' -end ',xmax,',',ymax,',',zmax,' -dim 0,1,2']);
unix(['sct_crop_image -i ',wm_fname,' -o wm_crop.nii.gz -start ',xmin,',',ymin,',',zmin,' -end ',xmax,',',ymax,',',zmax,' -dim 0,1,2']);
unix(['sct_crop_image -i ',M0_fname,' -o M0_crop.nii.gz -start ',xmin,',',ymin,',',zmin,' -end ',xmax,',',ymax,',',zmax,' -dim 0,1,2']);
unix(['sct_crop_image -i ',gm_fname,' -o gm_crop.nii.gz -start ',xmin,',',ymin,',',zmin,' -end ',xmax,',',ymax,',',zmax,' -dim 0,1,2']);
unix(['sct_crop_image -i ',csf_fname,' -o csf_crop.nii.gz -start ',xmin,',',ymin,',',zmin,' -end ',xmax,',',ymax,',',zmax,' -dim 0,1,2']);


t1=load_nii_data('T12template_crop.nii.gz');
t1(t1<0)=0;
M0=load_nii_data('M0_crop.nii.gz');
M0(M0<0)=min(M0(M0>0));
save_nii_v2(t1,'t1_ok.nii.gz','T12template_crop.nii.gz',64);
save_nii_v2(M0,'M0_ok.nii.gz','M0_crop.nii.gz',64);

%% Test N4BiasFieldCorrection

M0_original='M0.nii.gz';
M0=load_nii_data(M0_original);
M0(M0<0)=min(M0(M0>0));
save_nii_v2(M0,'M0_ok.nii.gz',M0_original,64);

for z=1:size(mask,3)
    if z>str2num(zmax) || z<str2num(zmin)
        mask(:,:,z)=0;
    end
end


correct_receive_profile_N4ITK('/Volumes/slevy/data/criugm/d_sp_pain_30_retest/mtv/M0.nii.gz', '/Volumes/slevy/data/criugm/d_sp_pain_30_retest/warping_fields/warp_mtv2template.nii.gz', '/Volumes/slevy/data/criugm/d_sp_pain_30_retest/warping_fields/warp_template2mtv.nii.gz', '/Volumes/slevy/data/criugm/d_sp_pain_30_retest/mtv/spgr20crop_csf.nii.gz', '/Volumes/slevy/data/criugm/d_sp_pain_30_retest/mtv/WF_n4.nii.gz', '/Volumes/slevy/data/criugm/d_sp_pain_30_retest/mtv/mtv_n4.nii.gz', '/Volumes/slevy/data/criugm/d_sp_pain_30_retest/template/M0_thr2template.nii.gz', '/Volumes/slevy/data/criugm/d_sp_pain_30_retest/template/M0_n42template.nii.gz', '/Volumes/slevy/data/criugm/d_sp_pain_30_retest/template/mtv_n42template', '/Volumes/slevy/data/criugm/d_sp_pain_30_retest/template/b1minus_n42template.nii.gz');

        
        





