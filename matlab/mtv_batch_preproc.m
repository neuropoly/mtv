%% -----------------------------------------Parameters to be updated-----------------------------------------------------

% Path to the MTV folder
%path_to_folder=/home/django/slevy/data/boston/hc_sc_003/mtv  % No need if the script is run from this folder

% Path to SPGR raw data
path_to_spgr4='/Volumes/data_shared/boston/connectome/HC_SC_003/dicom/8_fl3d_mtv_4/20140724_092151fl3dmtv4s008a1001.nii.gz';
path_to_spgr10='/Volumes/data_shared/boston/connectome/HC_SC_003/dicom/7_fl3d_mtv_10/20140724_092151fl3dmtv10s007a1001.nii.gz';
path_to_spgr20='/Volumes/data_shared/boston/connectome/HC_SC_003/dicom/6_fl3d_mtv_20/20140724_092151fl3dmtv20s006a1001.nii.gz';
path_to_spgr30='/Volumes/data_shared/boston/connectome/HC_SC_003/dicom/5_fl3d_mtv_30/20140724_092151fl3dmtv30s005a1001.nii.gz';

% Path to raw EPI for B1 mapping
path_to_ep60='/Volumes/data_shared/boston/connectome/HC_SC_003/dicom/9_ep_seg_se_FA60/20140724_092151epsegseFA60s009a1001.nii.gz';
path_to_ep120='/Volumes/data_shared/boston/connectome/HC_SC_003/dicom/10_ep_seg_se_FA120/20140724_092151epsegseFA120s010a1001.nii.gz';

% Coordinates to crop data
xmin=50;
xsize=80;
ymin=60;
ysize=80;
zmin=4;
zsize=13;

%% ----------------------------------------------------------------------------------------------------------------------
%% Copy files locally

% Go to mtv folder
%cd ${path_to_folder}  % No need if the script is run from this folder

% copy the data from the raw data
% SPGR
unix(['cp ' path_to_spgr4 ' spgr04.nii.gz'])
unix(['cp ' path_to_spgr10 ' spgr10.nii.gz'])
unix(['cp ' path_to_spgr20 ' spgr20.nii.gz'])
unix(['cp ' path_to_spgr30 ' spgr30.nii.gz'])
% B1 mapping
if ~exist('b1','dir'), mkdir('b1'); end % make folder B1 if doesn't exist
unix(['cp ' path_to_ep60 ' b1/ep60.nii.gz'])
unix(['cp ' path_to_ep120 ' b1/ep120.nii.gz'])

%% Preprocessing
% List of SPGR images
SPGR_LIST={'spgr4' 'spgr10' 'spgr20' 'spgr30'};

% Crop SPGR images
img=sct_tools_ls([SPGR_LIST{1} '.nii*'],1);
fslroi_input=sct_cropXY(img{1});
xmin=fslroi_input(1); xsize=fslroi_input(2); ymin=fslroi_input(3); ysize=fslroi_input(4); zmin=fslroi_input(5); zsize=fslroi_input(6);
for spgr=SPGR_LIST(2:end)
    unix(['fslroi ' spgr{1} ' ' spgr{1} '_crop ' num2str(xmin) ' ' num2str(xsize) ' ' num2str(ymin) ' ' num2str(ysize) ' ' num2str(zmin) ' ' num2str(zsize)]);
end
SPGR_LIST=cellfun(@(x) [x '_crop'], SPGR_LIST,'UniformOutput',0); % update fname

% SPGR segmentation
if ~exist('SC_seg.nii.gz','file')
    sct_get_centerline_manual([SPGR_LIST{end} '.nii.gz'])
    sct_unix(['sct_propseg -i ' SPGR_LIST{end} '.nii.gz -t t1 -radius 3 -init-centerline ' SPGR_LIST{2} '_centerline.nii']);
    sct_unix(['mv ' SPGR_LIST{end} '_seg.nii.gz SC_seg.nii.gz'])
end

% Create mask used for SPGR images registration
unix(['sct_create_mask -i ' SPGR_LIST{end} '.nii -m centerline,SC_seg.nii.gz -f cylinder -s 40'])

% Register all SPGR images to the SPGR image with flip angle=10
SPGR_LIST_reg={};
for spgr=SPGR_LIST
    sct_unix(['sct_register_multimodal -i ' spgr{1} '.nii* -d ' SPGR_LIST{1} '.nii -z 0 -p algo=slicereg,metric=MI,iter=20 -o ' spgr{1} '_reg.nii.gz'])
    SPGR_LIST_reg={SPGR_LIST_reg{:}, [spgr{1} '_reg']};
end
% merge SPGR
unix(['fslmerge -t SPGRs ' strjoin(SPGR_LIST_reg)])

% remove useless outputs
unix('rm warp_*')
%% Compute B1map
B1_LIST={'B1_FA60.nii.gz', 'B1_FA120.nii.gz'};
% Compute B1 profile
if ~exist('./b1','dir'), mkdir('b1'); end
sct_unix(['fslmaths -dt double B1_FA120.nii.gz -div 2 -div B1_FA60.nii.gz -acos -div ' num2str(60*pi/180) ' b1']);
% Reslice B1 in spgr space
sct_reslice('b1.nii.gz','SPGRs.nii.gz')

sct_unix('mtv_smooth_b1.py -i b1_reslice.nii')


%% Compute PD, T1 and MTVF maps
% FA=[4 10 20 30];
% mtv_compute_m0_t1(
% sct_mtv_compute.py -b b1/b1_smoothed_in_spgr10_space_crop.nii.gz -c spgr10_crop_csf_mask.nii.gz -f 4,10,20,30 -i spgr4to10_crop.nii.gz,spgr10_crop.nii.gz,spgr20to10.nii.gz,spgr30to10.nii.gz -o T1_map,PD_map,MTVF_map -p mean-PD-in-CSF-from-mean-SPGR -s ${fname_spgr10_seg} -t 0.02
%
% % create folder mtv/template if doesn't exist
% if ! [ -e "template" ]; then mkdir template; fi
%
% % Estimate warping field from template in anat space to spgr10_crop
% sct_register_multimodal -i ../t2/label/template/MNI-Poly-AMU_T2.nii.gz -d spgr10_crop.nii.gz -m mask_spgr10_crop.nii.gz -z 0 -p 5,sliceReg,1,MI
% mv warp_src2dest.nii.gz ../warping_fields/warp_template2mtv_step1.nii.gz
% mv warp_dest2src.nii.gz ../warping_fields/warp_mtv2template_step2.nii.gz
% rm MNI-Poly-AMU_T2_reg.nii.gz spgr10_crop_reg.nii.gz
%
% % Warp white matter to intermediate step to improve the registration afterwards
% sct_apply_transfo -i ../t2/label/template/MNI-Poly-AMU_WM.nii.gz -d spgr10_crop.nii.gz -w ../warping_fields/warp_template2mtv_step1.nii.gz -o template/wm2mtv_step1.nii.gz
%
% % Improve the registration
% sct_register_multimodal -i template/wm2mtv_step1.nii.gz -d T1_map.nii.gz -m mask_spgr10_crop.nii.gz -z 5 -p 5,BSplineSyN,0.1,MI -o template/wm2mtv_step2.nii.gz
% mv template/warp_src2dest.nii.gz ../warping_fields/warp_template2mtv_step2.nii.gz
% mv template/warp_dest2src.nii.gz ../warping_fields/warp_mtv2template_step1.nii.gz
% rm template/T1_map_reg.nii.gz
%
% % Concatenate warping fields to get:
% %   - warp_template2mtv_final = warp_template2anat_final + warp_template2anat_step1 + warp_template2anat_step2
% %   - warp_mtv2template_final = warp_mtv2template_step1 + warp_mtv2template_step2 + warp_anat2template_final
% sct_concat_transfo -w ../warping_fields/warp_template2anat_final.nii.gz,../warping_fields/warp_template2mtv_step1.nii.gz,../warping_fields/warp_template2mtv_step2.nii.gz -d spgr10_crop.nii.gz -o ../warping_fields/warp_template2mtv_final.nii.gz
% sct_concat_transfo -w ../warping_fields/warp_mtv2template_step1.nii.gz,../warping_fields/warp_mtv2template_step2.nii.gz,../warping_fields/warp_anat2template_final.nii.gz -d ${SCT_DIR}/data/template/MNI-Poly-AMU_T2.nii.gz -o ../warping_fields/warp_mtv2template_final.nii.gz
%
% % Warp atlas to MTV space
% sct_warp_template -d spgr10_crop.nii.gz -w ../warping_fields/warp_template2mtv_final.nii.gz
%
% % Warp MTVF map to template
% sct_apply_transfo -i MTVF_map.nii.gz -o MTVF_map_to_template.nii.gz -d ${SCT_DIR}/data/template/MNI-Poly-AMU_T2.nii.gz -w ../warping_fields/warp_mtv2template_final.nii.gz -p spline