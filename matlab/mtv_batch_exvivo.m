function mtv_batch_exvivo(SPGR_LIST,B1_LIST,flipAngles,TR,preproc)
%% Preprocessing
% Crop SPGR images
for i=1:length(SPGR_LIST), save_nii_v2(load_nii(SPGR_LIST{i}),SPGR_LIST{i}); end
if preproc
    fslroi_input=sct_cropXY(SPGR_LIST{1},'autobox','30');
    xmin=fslroi_input(1); xsize=fslroi_input(2); ymin=fslroi_input(3); ysize=fslroi_input(4); zmin=fslroi_input(5); zsize=fslroi_input(6);
    for spgr=SPGR_LIST(2:end)
        sct_unix(['fslroi ' spgr{1} ' ' spgr{1} '_crop ' num2str(xmin) ' ' num2str(xsize) ' ' num2str(ymin) ' ' num2str(ysize) ' ' num2str(zmin) ' ' num2str(zsize)]);
    end
    SPGR_LIST=cellfun(@(x) [x '_crop'], SPGR_LIST,'UniformOutput',0); % update fname
    
    
    ref_nb=ceil(length(SPGR_LIST)/2+0.5);
    % SPGR segmentation
    if ~exist('SC_seg.nii.gz','file')
        sct_get_centerline_manual([SPGR_LIST{ref_nb}])
        save_nii_v2(load_nii_data([SPGR_LIST{ref_nb}]),[SPGR_LIST{ref_nb}],[SPGR_LIST{ref_nb}])
        sct_unix(['sct_propseg -i ' SPGR_LIST{ref_nb} ' -t t1 -radius 3 -init-centerline ' sct_tool_remove_extension(SPGR_LIST{ref_nb},1) '_centerline.nii']);
        sct_unix(['mv ' sct_tool_remove_extension(SPGR_LIST{ref_nb},1) '_seg.nii.gz SC_seg.nii.gz'])
    end
    
    sct_unix(['fslview ' SPGR_LIST{ref_nb} ' SC_seg.nii.gz -l Red -t 0.5'])
    
    % Create mask used for SPGR images registration
    %unix(['sct_create_mask -i ' SPGR_LIST{end} '.nii -m centerline,SC_seg.nii.gz -f cylinder -s 40'])
    
    % Register all SPGR images to the SPGR image with flip angle=10
    SPGR_LIST_reg={}; sct_unix(['sct_image -setorient RPI -i ' SPGR_LIST{ref_nb} ' -o ' SPGR_LIST{ref_nb}]);
    for spgr=SPGR_LIST
        if ~strcmp(spgr{1},SPGR_LIST{ref_nb})
            sct_unix(['sct_register_multimodal -i ' spgr{1} ' -d ' SPGR_LIST{ref_nb} ' -z 0 -p algo=slicereg,metric=MI,iter=20 -o ' spgr{1} '_reg.nii.gz'])
            SPGR_LIST_reg={SPGR_LIST_reg{:}, [sct_tool_remove_extension(spgr{1},1) '_reg']};
        else SPGR_LIST_reg={SPGR_LIST_reg{:}, spgr{1}};
        end
    end
    sct_unix(['rm ' SPGR_LIST{ref_nb} '_reg*']);
    
    % remove useless outputs
    unix('rm warp_*')
else
    SPGR_LIST_reg=SPGR_LIST;
end

% merge SPGR
sct_unix(['fslmerge -t SPGRs ' strjoin(SPGR_LIST_reg)])
%% Compute B1map
% Compute B1 profile
sct_unix(['fslmaths -dt double ' B1_LIST{2} ' -div 2 -div ' B1_LIST{1} ' -acos -div ' num2str(60*pi/180) ' b1']);
% Reslice B1 in spgr space
sct_reslice('b1.nii.gz','SPGRs.nii.gz')

%%

%% COMPUTE MTV
%% inputs
SPGR=load_nii('SPGRs.nii'); %SPGR.img=double(SPGR.img)./repmat(reshape([80 41 23 17],[1 1 1 4]),[SPGR.dims(1:3)]);
b1Map=load_nii_data('b1_reslice.nii'); b1mask=~~b1Map;
b1Map=mtv_fit3dpolynomialmodel(b1Map,b1mask,6);
seg='SC_seg.nii.gz'; if ~exist(seg,'var'), seg=sct_create_mask('SPGRs.nii'); end
seg_nii=load_nii(seg);


%% Launch MTV
[M0, t1Biased] = mtv_compute_m0_t1(double(SPGR.img), flipAngles, TR, b1Map); %,[],0,1);
save_nii_v2(M0(:,:,:,1),'M0','SPGRs.nii.gz')
save_nii_v2(t1Biased(:,:,:,1),'T1','SPGRs.nii.gz',64)

%% create csf mask
% csf mask using T1 values
csf_mask=t1Biased(:,:,:,1);
csf_mask(csf_mask<1 | csf_mask>2)=0;
% csf mask using seg
SE=translate(strel('disk',floor(4/seg_nii.scales(1)),4),[0 3]);
seg_csf=logical(imdilate(seg_nii.img,SE)-seg_nii.img);
% compose
seg_csf=csf_mask & seg_csf;
figure,imagesc3D(seg_csf)
save_nii_v2(seg_csf,'CSF_seg',seg);


%% correct reception profile
mtv_correct_receive_profile M0.nii T1.nii SC_seg.nii.gz CSF_seg.nii
mtv=load_nii_data('mtv.nii.gz');
mtv_cor=mtv.*mean(mtv(seg_nii.img & b1mask))./mtv_fit3dpolynomialmodel(mtv,seg_nii.img & b1mask,1);
mtv_cor(~seg_nii.img)=mtv(~seg_nii.img);
save_nii_v2(mtv_cor,'mtv_cor','mtv.nii.gz')
sct_unix('fslview mtv_cor.nii -l /Users/taduv_admin/luts/renderjet_julien.lut -b 0,0.5 mtv -l /Users/taduv_admin/luts/renderjet_julien.lut -b 0,0.5')
%k=fminsearch(@(x) std(M0(logical(seg_csf))./b1Map.img(logical(seg_csf)).^x)/mean(M0(logical(seg_csf))./b1Map.img(logical(seg_csf)).^x), 2)