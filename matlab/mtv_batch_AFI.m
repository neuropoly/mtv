sct_unix('mv tmp* mtv_DAM')
tmpdir='mtv_AFI'; mkdir(tmpdir);
copyfile('SPGRs.nii.gz',tmpdir)
copyfile('mtv_DAM/SPGRs_crop_ref_seg.nii.gz',tmpdir)
%%
B1file = sct_tools_ls('B1_AFI*.nii*'); B1file = B1file{1};
B1angle = sscanf(B1file,'B1_AFI_FA%d');
sct_gunzip(B1file,tmpdir,'b1_AFI.nii');
cd(tmpdir)
% b1 = load_untouch_nii('b1_AFI.nii');
% b1 = double()./
% save_nii_v2(b1,[tmpdir filesep 'b1'],B1file,64);
% copyfile('B1_DAM.nii.gz',tmpdir)
%cd(tmpdir)
%% 
ref=2;

% crop
sct_cropXY SPGRs.nii.gz autobox 30
%moco
sct_unix(['fslroi SPGRs_crop.nii SPGRs_crop_ref.nii ' num2str(ref-1) ' 1'])
sct_moco('SPGRs_crop.nii',ref)

% segment cord
% sct_get_centerline_manual SPGRs_crop_ref.nii.gz
% save_nii_v2(load_nii('SPGRs_crop_ref.nii.gz'),'SPGRs_crop_ref.nii.gz')
% sct_unix(['sct_propseg -i SPGRs_crop_ref.nii.gz -t t1 -radius 3 -init-centerline SPGRs_crop_ref_centerline.nii']);
% sct_unix(['fslview SPGRs_crop_ref.nii SPGRs_crop_ref_seg.nii.gz -l Red -t 0.5'])


%% Compute B1map
% split
% B1_LIST = sct_tools_ls('FA*.nii*');
sct_reslice b1_AFI.nii SPGRs_crop_ref.nii.gz;

% smooth b1
b1Map=load_nii_data('b1_AFI_reslice.nii'); b1Map = b1Map/B1angle;
b1Map=mtv_fit3dpolynomialmodel(b1Map,b1Map>0.5 & b1Map<1.5,6); save_nii_v2(b1Map,'b1_AFI_smooth.nii','b1_AFI_reslice.nii')

%% compute T1 and M0
[M0, t1] = mtv_compute_m0_t1(double(load_nii_data('SPGRs_crop_moco.nii')), [4 10 20], 0.03, b1Map); %,[],0,1);
save_nii_v2(M0(:,:,:,1),'M0','SPGRs.nii.gz')
save_nii_v2(t1(:,:,:,1),'T1','SPGRs.nii.gz',64)

%% create csf mask
% csf mask using T1 values
csf_mask=t1(:,:,:,1);
csf_mask(csf_mask<2 | csf_mask>5)=0;
% use cord seg
seg_nii=load_nii('SPGRs_crop_ref_seg.nii.gz');
% csf mask using seg
SE=translate(strel('disk',floor(4/seg_nii.scales(1)),4),[0 3]);
seg_csf=logical(imdilate(seg_nii.img,SE)-seg_nii.img);
% compose
seg_csf=csf_mask & seg_csf;
figure,imagesc3D(seg_csf)
save_nii_v2(seg_csf,'CSF_seg','SPGRs_crop_ref_seg.nii.gz');


%% compute PD
% metrics={'M0' 'T1' 'SPGRs_crop_ref_seg' 'CSF_seg'};
% for im=1:length(metrics)
% sct_unix(['fslroi ' metrics{im} ' ' metrics{im} '_Z4_18 0 -1 0 -1 3 15']);
% end
mtv_correct_receive_profile M0.nii T1.nii SPGRs_crop_ref_seg.nii.gz CSF_seg.nii
sct_unix(['fslroi mtv mtv_Z4_18 0 -1 0 -1 3 15']);
sct_unix(['fslroi T1 T1_Z4_18 0 -1 0 -1 3 15']);


%% copy back
cd ..
copyfile(['mtv_AFI' filesep 'mtv_Z4_18.nii.gz'],'./mtv_AFI.nii.gz')
copyfile(['mtv_AFI' filesep 'T1_Z4_18.nii.gz'],'./T1_AFI.nii.gz')

%% plot
figure(43)
AFI = load_nii_data('mtv_AFI/b1_AFI_smooth.nii');
DAM = load_nii_data('mtv_DAM/b1_reg_crop_smooth.nii');
seg=~~load_nii_data('mtv_DAM/SPGRs_crop_ref_seg.nii.gz');
seg2=seg; seg2(:,:, setdiff(1:size(seg,3),[5:15]))=0; plot(DAM(seg2),AFI(seg2),'+','Color',[rand rand rand]);