function [C, PAPA] = depends(f,depth)
  % DEPENDS Computes dependencies for a given function, f.
  %
  % C = depends(f)
  % C = depends(f,depth)
  %
  % Inputs:
  %   f  string, name of function or path to function
  %   depth  number of levels to explore {Inf}
  % Outputs:
  %   C  cell array of paths to dependencies
  %
  % Example:
  %  % Get dependencies and zip into a file
  %  C = depends('myfun');
  %  zip('myfun.zip',C);
  %
  %  % Get dependencies and extract just file base names and print
  %  C = depends('myfun');
  %  N = regexprep(C,'^.*\/','')
  %  fprintf('myfun depends on:\n');
  %  fprintf('  %s\n',N{:})
  %
  %  % Get dependencies and remove mosek paths
  %  C = depends('myfun');
  %  C = C(cellfun(@isempty,strfind(C,'opt/local/mosek')));
  % 
  
  w = warning('off','MATLAB:DEPFUN:DeprecatedAPI');
  

  if(~exist('depth','var'))
    depth = Inf;
  end

  level = 0;
  C = {};
  PAPA = {};
  C{end+1} = which(f);
  PAPA{end+1} = which(f);
    
  Q = {};
  Q = C;
  while( level < depth && ~isempty(Q))
    new_deps = {};
    parentfolder = {};
    while(~isempty(Q))
      p = Q{1};
      Q = Q(2:end);
      newtmp=depfun(p,'-toponly','-quiet');
      new_deps = cat(1,new_deps,newtmp);
      parentfolder = cat(1,parentfolder,repmat({p},[length(newtmp) 1]));
      % This is the non-obsolete version but it's 100x slower : - (
      %new_deps = cat(1,new_deps, ...
        %matlab.codetools.requiredFilesAndProducts(p,'toponly')');C
    end
    % ignore anything in matlab folder
    parentfolder = parentfolder(cellfun(@isempty,strfind(new_deps,matlabroot)));
    new_deps = new_deps(cellfun(@isempty,strfind(new_deps,matlabroot)));
    [Q, in] = setdiff(new_deps,C);
    parentfolder = parentfolder(in);
    if ~isempty(Q)
      C = cat(1,C,Q);
      PAPA = cat(1,PAPA,parentfolder);
    end
    level= level + 1;
  end
  
  warning(w);
  
end
