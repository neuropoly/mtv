function [ll_T1]=mtv_fitT1_IR(data,T_IR,method)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function estimates T1 using IR data.
%
% INPUT VARIABLES :
%     -data:  Merged IR data. Taken from the .nii file of the merged IR.
%             Here is an example of how to create the merged IR file :
%                 data=double(load_nii_data('IR_merged.nii'));
%             To create that 'IR_merged.nii' file, you can follow the
%             example below :
%                 dicm2nii mp2rage/06-ep_seg_se_IR50ms_concat2 ./;
%                 dicm2nii mp2rage/07-ep_seg_se_IR419ms_concat2 ./;
%                 dicm2nii mp2rage/08-ep_seg_se_IR800ms_concat2 ./;
%                 dicm2nii mp2rage/09-ep_seg_se_IR1200ms_concat2 ./;
%                 dicm2nii mp2rage/10-ep_seg_se_IR2200ms_concat2 ./;
%                 SEIR_LIST=sct_tools_ls('ep_seg_se_IR*');
%                 SEIR(:,:,:,1) = double(load_nii_data(SEIR_LIST{1}));
%                 SEIR(:,:,:,2) = double(load_nii_data(SEIR_LIST{2}));
%                 SEIR(:,:,:,3) = double(load_nii_data(SEIR_LIST{3}));
%                 SEIR(:,:,:,4) = double(load_nii_data(SEIR_LIST{4}));
%                 SEIR(:,:,:,5) = double(load_nii_data(SEIR_LIST{5}));
%                 save_nii_v2(SEIR,'IR_merged',SEIR_LIST{1},64);
%     -T_IR:  Array containing the IR times, in ms. For example, using the
%             same example as above : 
%                 T_IR=[50 419 800 1200 2200];
%     -method:Method to use in order to fit the data. There are two
%             available options which estimate T1 together with:
%                 'RD-NLS'   : ??? fit. Uses a and b parameters to
%                              fit the data to a + b*exp(-TI/T1)
%             or  'RD-NLS-PR': ??? fit. Uses c and d parameters to 
%                              fit the data to |c + d*exp(-TI/T1)|
%
% OUTPUT VARIABLES :
%     -ll_T1: T1 map estimated from the IR data using the chosen method.
%             It can be saved to a nii file using the following function
%             using the header of the 'IR_merged.nii' file (using the same
%             example as above) : 
%                 save_nii_v2(ll_T1(:,:,:,1),'T1_IR','IR_merged.nii',64);
%
% Original function T1ScanExperiment.m written by : 
%     J. Barral, M. Etezadi-Amoli, E. Gudmundson, and N. Stikov, 2009
% Edited for direct use of data by :
%     A. Daigle-Martel for direct use of available data, 2016
%
% (c) Board of Trustees, Leland Stanford Junior University
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    extra.tVec = T_IR;
    extra.T1Vec = 1:5000; % Range can be reduced if a priori information is available
    
    savefitdata = 0; % Can be changed to 1 if the fit data needs to be saved

    nlsS = getNLSStruct(extra,1);
    
    switch method
      case{'RD-NLS'}
        nbrOfFitParams = 4; % Number of output arguments for the fit
        fitStr = ['[T1Est bEst aEst res] = rdNls(data(jj,:),nlsS);']
        storeStr = 'll_T1(jj,:) = [T1Est bEst aEst res];'
        clearStr = 'clear jj T1Est bEst aEst res'
      case{'RD-NLS-PR'}
        data = abs(data);
        nbrOfFitParams = 4; % Number of output arguments for the fit
        fitStr = ['[T1Est bMagEst aMagEst res] = rdNlsPr(data(jj,:),nlsS);']
        storeStr = 'll_T1(jj,:) = [T1Est bMagEst aMagEst res];'
        clearStr = 'clear jj T1Est bMagEst aMagEst res'
    end

    dims = size(data);
    nbrow = size(data,1);
    nbcol = size(data,2);

    if numel(dims) > 3
        nbslice = dims(3); % Check number of slices 
    else
        nbslice = 1;
        tmpData(:,:,1,:) = data; % Make data a 4-D array regardless of number of slices
        data = tmpData;
        clear tmpData;
    end

    % The data needs to lie along an exponential.
    % Refer to getData.m if that is not the case.
    while (true)
        if (nbslice ~= 1)
            disp('For which slice would you like to check the data?');
            zz = input(['Enter number 1 to ' num2str(nbslice) '.  0 for no check --- '], 's');
            zz = cast(str2num(zz), 'int16');
            if (isinteger(zz) & zz >= 0 & zz <= nbslice) break;
            end
        else
            zz = input(['Enter 1 to check the data, 0 for no check --- '], 's');
            zz = cast(str2num(zz), 'int16');
            if (isinteger(zz) & zz >= 0 & zz <= nbslice) break;
            end
        end	
    end

    if (zz ~= 0)
        sliceData = squeeze(data(:,:,zz,:));
        disp('Click on one point to check that the time series looks like an exponential')
        disp('Refer to getData.m if that is not the case.  CTRL-click or right-click when done');
        TI = extra.tVec;
        plotData(real(sliceData),TI);
    end

    close all

    dataOriginal = data;

    % Mask the background (points dimmer than maskFactor*the brightest point) 
    % Done on the last image, where we expect the magnetization to have almost
    % fully recovered. 
    % Increase mF is the mask does not cover all the background. Decrease it if
    % it covers part of the object. 
    mF = 0.1;
    maskFactor = mF;
    mask = zeros(nbrow, nbcol, nbslice);

    [u,v] = max(extra.tVec);

    for kk = 1:nbslice
        maskTmp = mask(:,:,kk);
        maskTmp = medfilt2(maskTmp); % remove salt and pepper noise
        maskThreshold = maskFactor*max(max(abs(data(:,:,kk,v))));
        maskTmp(find(abs(data(:,:,kk,v))> maskThreshold)) = 1;
        mask(:,:,kk) = maskTmp;
        clear maskTmp
    end

    figure, 
    imshow(abs(mask(:,:,round(end/2))),[])

    disp('Check that maskFactor is appropriate. Hit enter if so. Cancel and adjust mF in T1ScanExperiment.m if not.')
    pause

    maskInds = find(mask);

    nVoxAll = length(maskInds);
    % How many voxels to process before printing out status data
    numVoxelsPerUpdate = min(floor(nVoxAll/10),1000); 

    ll_T1 = zeros(nVoxAll, nbrOfFitParams);
    % Number of status reports
    nSteps = ceil(nVoxAll/numVoxelsPerUpdate); 

    for ii = 1:size(data,4)
        tmpVol = data(:,:,:,ii);
        tmpData(:, ii) = tmpVol(maskInds)';
    end
    clear tmpVol;
    data = tmpData;
    clear tmpData;

    startTime = cputime;
    fprintf('Processing %d voxels.\n', nVoxAll);

    h = waitbar(0, sprintf('Processing %d voxels', nVoxAll)); 

    for ii = 1:nSteps
      curInd = (ii-1)*numVoxelsPerUpdate+1;
      endInd = min(curInd+numVoxelsPerUpdate,nVoxAll);
      for jj = curInd:endInd
        % Do the fit
        eval(fitStr) 
        % Store the data
        eval(storeStr);
      end
      waitbar(ii/nSteps, h, sprintf('Processing %d voxels, %g percent done...\n',nVoxAll,round(endInd/nVoxAll*100)));
    end
    eval(clearStr)
    close(h);
    timeTaken = round(cputime - startTime);
    fprintf('Processed %d voxels in %g seconds.\n',nVoxAll, timeTaken);

    dims = [size(mask) 4];
    im = zeros(size(mask));

    for ii = 1:nbrOfFitParams
        im(maskInds) = ll_T1(:,ii);
        T1(:,:,:,ii) = im;
    end

    % Going back from a numVoxels x 4 array to nbrow x nbcol x nbslice
    ll_T1 = T1;

    % Store ll_T1 and mask in saveStr
    % For the complex data, ll_T1 has four parameters 
    % for each voxel, namely:
    % (1) T1 
    % (2) 'b' or 'rb' parameter 
    % (3) 'a' or 'ra' parameter
    % (4) residual from the fit

    if (savefitdata)
        save(saveStr,'ll_T1','mask','nlsS')
    end

    % Check the fit
    TI = extra.tVec;
    nbtp = 20;
    timef = linspace(min(TI),max(TI),nbtp);

    % Inserting a short pause, otherwise some computers seem
    % to get problems
    pause(1);

    zz = 0;
    while(true)
      if (nbslice ~= 1)
        disp('For which slice would you like to check the fit?');
        zz = input(['Enter number 1 to ' num2str(nbslice) '.  0 for no check --- '], 's');
        zz = cast(str2num(zz), 'int16');
        if (isinteger(zz) && zz >= 0 && zz <= nbslice) 
          break;
        end
      else
        zz = input(['Enter 1 to check the fit, 0 for no check --- '], 's');
        zz = cast(str2num(zz), 'int16');
        if (isinteger(zz) && zz >= 0 && zz <= nbslice) 
          break;
        end
      end
    end

    if (zz ~= 0)
      sliceData = squeeze(dataOriginal(:,:,zz,:));
      datafit = zeros(nbrow,nbcol,nbtp);
      switch method
        case{'RD-NLS'}
            for kk = 1:nbtp
                datafit(:,:,kk) = ll_T1(:,:,zz,3) + ...
                    ll_T1(:,:,zz,2).*exp(-timef(kk)./ll_T1(:,:,zz,1));
            end
        case{'RD-NLS-PR'}
            for kk = 1:nbtp
                datafit(:,:,kk) = abs(ll_T1(:,:,zz,3) + ...
                    ll_T1(:,:,zz,2).*exp(-timef(kk)./ll_T1(:,:,zz,1)));
            end
      end
      disp('Click on one point to check the fit. CTRL-click or right-click when done')
      plotData(real(sliceData),TI,real(datafit),ll_T1); 
      close all
    end
end