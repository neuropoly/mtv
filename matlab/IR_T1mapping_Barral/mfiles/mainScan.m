% mainScan.m
%
% Loads .mat file from the directory 'data' in the T1path, 
% performs T1 mapping, and displays the results 
%
% written by J. Barral, M. Etezadi-Amoli, E. Gudmundson, and N. Stikov, 2009
%  (c) Board of Trustees, Leland Stanford Junior University

%clear all
%close all

T1path = './';

data=double(load_nii_data('TI_merged.nii.gz'));
extra.tVec = [50 419 800 1200 2200];
extra.T1Vec = 1:5000; % this range can be reduced if a priori information is available
save data data extra

%% Where to find the data
loadpath = [T1path];

datasetnb = 2;

switch (datasetnb)
	case 1
		filename = 'data'; % complex fit
		method = 'RD-NLS'
	case 2
		filename = 'data'; % magnitude fit
		method = 'RD-NLS-PR'
end


%% Where to save the data
savepath = [T1path 'fitdata/'] 

loadStr = [loadpath filename]
saveStr = [savepath 'T1Fit' method '_' filename]
mkdir fitdata
%% Perform fit
T1ScanExperiment(loadStr, saveStr, method);

%% Display results
T1FitDisplayScan(loadStr, saveStr);

%% save
load('fitdata/T1FitRD-NLS-PR_data.mat', 'll_T1')
save_nii_v2(ll_T1(:,:,:,1),'T1_IR','TI_merged.nii.gz',64);
