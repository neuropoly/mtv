% the inPut data

% Mask=readFileNifti('/Users/avivmezer/Documents/reaserch/SpinalCore/recorrectingmtvmaps/spgr20crop_seg.nii.gz');
Mask=readFileNifti('/Volumes/slevy-4/data/criugm/d_sp_pain_pilot8/mtv/scan/spgr20to10_seg.nii.gz');
Mask=logical(Mask.data);
CSF=readFileNifti('/Volumes/slevy-4/data/criugm/d_sp_pain_pilot8/mtv/scan/spgr20to10_csf_mask.nii.gz');
xform= CSF.qto_xyz;
CSF=logical(CSF.data);
A=readFileNifti('/Volumes/slevy-4/data/criugm/d_sp_pain_pilot8/mtv/scan/spgr20to10.nii.gz');

T1=readFileNifti('/Users/slevy_local/mtv/matlab/spgr_fit_ganter/results/T1.nii.gz');
T1=T1.data;
%% put it in order 
NiName={'spgr4to10.nii.gz', 'spgr10crop.nii.gz','spgr20to10.nii.gz'};
FA=[4 10 20];
outDir='/Volumes/slevy-4/data/criugm/d_sp_pain_pilot8/mtv/scan/b1_fitting';

for i=1:length(FA)
    FileName=fullfile(outDir,NiName{i})
    tmp.flipAngle=FA(i);
    tmp.TE=5.92;
    tmp.TR=35;
    tmp.fieldStrength=3;
    tmp=makeStructFromNifti(FileName,[],tmp);
    s(i)=tmp;
end
%%
tr=35;
flipAngles =   [ s(:).flipAngle];

B1 = ones(size(s(1).imData));

[t1,M0]    = relaxFitT1(double(cat(4,s(:).imData)),flipAngles,tr,B1);
%%
for i=1:4
    MCSF(i)=mean(double(s(i).imData(CSF)));
    SCSF(i)=std(double(s(i).imData(CSF)));
        MMask(i)=mean(s(i).imData(Mask));
end

Res{1}.im=double(CSF).*4300;
Res{2}.im=T1;
for i=1:4
Res{i+2}.im=s(i).imData;
end
%%
    outDir='/Users/avivmezer/Documents/reaserch/SpinalCore/recorrectingmtvmaps/output';
    sub='test';
    
    M=CSF + Mask;
    Maskfile=fullfile(outDir,'Mask.nii.gz')
    dtiWriteNiftiWrapper(single(M), xform, Maskfile);

    
    %%
[B1 resNorm dd] = mrQ_fitB1_LSQ(Res, CSF, tr,FA, outDir, mean(double(M0(CSF))), xform, 0, 1, [sub 'B1fit'],0);
%% smooth B1
 B1epifile=fullfile(outDir,'B1_lsq_last.nii.gz');
 B1epiResidfile=fullfile(outDir,'lsqT1PDresnorm_last.nii.gz');
  B1file=fullfile(outDir,'B1_map.nii.gz');

[B1]=mrQ_smmothL_B1_spin(B1epifile,B1epiResidfile,B1file,xform);


B1(~M)=1;
%% correctet T1
    [t11,M01]    = relaxFitT1(double(cat(4,s(:).imData)),flipAngles,tr,B1);
  T1file=fullfile(outDir,'T1_B1_map.nii.gz');
  M0file=fullfile(outDir,'M0_B1_map.nii.gz');

dtiWriteNiftiWrapper(single(t11), xform, T1file);
dtiWriteNiftiWrapper(single(M01), xform, M0file);

%% fitting PD

%% masking

mask=tt1<3 & tt1>0.4 & M01>500 & M01<4000 & s(1).imData>40;
[mask1] = ordfilt3D(mask,5); % clean


Maskfile=fullfile(outDir,'Biger_Mask.nii.gz');
dtiWriteNiftiWrapper(single(mask1), xform, Maskfile);

%BMfile=Maskfile;
MM=(Mask+mask1+CSF); MM1=MM;
MM1(:,1:36,:)=0; MM1(40:end,:,:)=0;MM1(1:15,:,:)=0;

Maskfile1=fullfile(outDir,'Area_Mask.nii.gz');
dtiWriteNiftiWrapper(single(MM1), xform, Maskfile1);

%%
% outDir ='PATH TO DIR' %output directory path
dir='/Users/avivmezer/Documents/reaserch/SpinalCore/recorrectingmtvmaps' %the top lever dir
%%
%% run the fit number of input coils --> in our case 1.
Coilsinfo.maxCoil=1;Coilsinfo.minCoil=1; Coilsinfo.useCoil=1;
Reg=5;Init=5; % The method of fitting. in our case No 5. this is the local T1.
 
 
 
 %%
% Make an output directory
    if ~exist(outDir,'dir'); mkdir(outDir); end
    % MAke a output structure.
            mrQ = mrQ_Create(dir,[],outDir);
            
%  We are using code for mrQ software. The code also fit B1 and T1 etc. 
%   Let's skip all irrelevant mrQ processing until the PD and coil
% sensitivity mapping. So we define some bookkeeping parameters manually.
            mrQ.Arange_Date=date;
            mrQ.SEIR_done=1;
            mrQ.SPGR_init_done=1;
            mrQ.SPGR_coilWeight_done=1;
            mrQ.SPGR_T1fit_done=1;
            mrQ.segmentaion=1;
            mrQ.calM0_done=1;
            mrQ.calM0_done=1;
            mrQ.brakeAfterPD=1;
    mrQ.SunGrid=0;
        mrQ.proclus=0;
mrQ.spgr_initDir=outDir;
 
% Let?s save the M0 image
mrQ.M0combineFile=M0file;
% Same 
save(mrQ.name,'mrQ');

%% Get all the fit parameters
% Generate the details for the local Gain (and PD) fit. The idea is that
% the M0 is separated to many overlap areas.
% We use here default parameters box-size (size of local area
% we fit = 14mm)  and resolution (the image resolution ?it is possible to bluer it (to fit the sensitivity ), overlap between each local fit area = 50%). it might be useful to
% optimize that.
 %boxsize=8;
 boxsize=9;
outMm=0% We replace the data to this low resolution for speed. since we only fit smooth gain any way. you can change it. To  keep the original resolution change the input to  outMm=0;
Inclusion_Criteria=[0 60];

[mrQ.opt_logname]=mrQ_PD_multicoil_RgXv_GridCall_Fittesting(mrQ.spgr_initDir,mrQ.sub,mrQ.PolyDeg,M0file,T1file,Maskfile1,outMm,boxsize,[],Coilsinfo,Reg,Init,[],Inclusion_Criteria,mrQ);
 
save(mrQ.name,'mrQ');
%%
% fit each local M0 area (with T1 input). 
mrQ_fitM0boxesCall_T1PD(mrQ.opt_logname)
 
 
%% Build the local fits
%Join the local overlap area to one PD image.
  mrQ.opt=mrQ_buildPD_ver2(mrQ.opt_logname,0,[],[],[],0.01);
 
