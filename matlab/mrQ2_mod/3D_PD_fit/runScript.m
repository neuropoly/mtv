

%% A script that runs the fit and build the PD map
% This script run the fit (many local fits) and then join them together.

% To run this you need to add these git repositories to your matlab path. you can do it by:  addpath(genpath('gitPath'));
%mrQ - https://github.com/mezera/mrQ
%Vistasoft - https://github.com/vistalab/vistasoft
%KNKUtils (from Kendrick Kay) - https://github.com/kendrickkay/knkutils
% You also need to install SPM.

% Aviv Mezer (C) Hebrew University, 2015
%

%%
% path to the input nifti files
% please use the right path in your computer
%exmpale 1
% BMfile='~/mask.nii.gz';
BMfile='./spgr20to10_seg.nii.gz'; % mask file where you got data to fit PD with reliable T1
% BMfile='./full_mask.nii.gz';
T1file='./T1_3d.nii.gz';
M0file='./M0_3d.nii.gz';
outDir ='aviv_code_results' %output directory path
dir='./' %the top lever dir
%%
%% run the fit number of input coils --> in our case 1.
Coilsinfo.maxCoil=1;Coilsinfo.minCoil=1; Coilsinfo.useCoil=1;
Reg=5;Init=5; % The method of fitting. in our case No 5. this is the local T1.
 
%%
% Make an output directory
if ~exist(outDir,'dir'); mkdir(outDir); end
% MAke a output structure.
mrQ = mrQ_Create(dir,[],outDir);

%  We are using code for mrQ software. The code also fit B1 and T1 etc. 
%   Let's skip all irrelevant mrQ processing until the PD and coil
% sensitivity mapping. So we define some bookkeeping parameters manually.
mrQ.Arange_Date=date;
mrQ.SEIR_done=1;
mrQ.SPGR_init_done=1;
mrQ.SPGR_coilWeight_done=1;
mrQ.SPGR_T1fit_done=1;
mrQ.segmentaion=1;
mrQ.calM0_done=1;
mrQ.calM0_done=1;
mrQ.brakeAfterPD=1;
mrQ.SunGrid=0;
mrQ.proclus=0;
mrQ.spgr_initDir=outDir;
 
% Let's save the M0 image
mrQ.M0combineFile=M0file;
% Same 
save(mrQ.name,'mrQ');
 
 
%% Get all the fit parameters
% Generate the details for the local Gain (and PD) fit. The idea is that
% the M0 is separated to many overlap areas. We use here default parameters
% box-size (size of local area we fit = 14mm)  and resolution (the image
% resolution ?it is possible to bluer it (to fit the sensitivity ), overlap
% between each local fit area = 50%). it might be useful to optimize that.
 boxsize=[9,6,8];
% boxsize=[];
% outMm=[2 2 2]% We replace the data to this low resolution for speed. since we only fit smooth gain any way. you can change it. To  keep the original resolution change the input to  outMm=0;
outMm=0;
[mrQ.opt_logname]=mrQ_PD_multicoil_RgXv_GridCall_Fittesting(mrQ.spgr_initDir,mrQ.sub,mrQ.PolyDeg,M0file,T1file,BMfile,outMm,boxsize,0.5,Coilsinfo,Reg,Init,[],mrQ);
%# perform k-means clustering on T1 map in the defined brain mask, using 3 clusters and excluding voxels with T1 higher than 1/0.35(=2.9)s. Boxes are defined according to 'boxsize' and voxel dimensions of the T1 data.
save(mrQ.name,'mrQ');
%%
% fit each local M0 area (with T1 input). 
mrQ_fitM0boxesCall_T1PD(mrQ.opt_logname)
%# It seems that only a few boxes are used (for the cord) because of the small size of the cord
%% Build the local fits
%Join the local overlap area to one PD image.
mrQ.opt=mrQ_buildPD_ver2(mrQ.opt_logname,0,[],[],[],0.01);
 
 


