function mrQ_fitM0boxesCall_T1PD(opt_logname,min_lim_for_box, RunSelectedJob)


load (opt_logname);
dirname=opt.dirname;
jumpindex=opt.jumpindex ;

if (~exist(dirname,'dir')),
        mkdir(dirname);
end
        
jumpindex=length(opt.wh);
opt.jumpindex=jumpindex;

mrQ_CoilPD_gridFit_PD_T1(opt,jumpindex,1,min_lim_for_box)
save(opt.logname,'opt');