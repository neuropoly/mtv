function [PD_fit, opt]= mrQ_BoxJoinBox(Boxes,Cbox,opt,BMfile)
%[PD_fit]= mrQ_BoxJoinBox(Boxes,Cbox,SHub,opt);
% % join the boxes (Boxes) acording to the box Costats (Cbox) calculate by
% mrQ_boxScaleGlobLinear.m .
% make a PD image from all those BOXES PD PD_FIT.

%the box that we have estimations for
wh=find(Cbox);

if notDefined('BMfile')
    BMfile=opt.BMfile;
end

% BM=readFileNifti(BMfile);  
% xform=BM.qto_xyz;
% BM=BM.data;
BM_nii=load_nii(BMfile,[],[],[],[],[],1);  
% xform=BM.qto_xyz;
BM=BM_nii.img;
SZ=size(BM);

% the box we will work on
% M0 matrix (voxels,max number of PD estimation of each voxel)
M0=zeros(length(BM(:)),32);
clear BM;
%loop over the box and add the value for each voxel. we have keep the multipal voxels values
for ii=wh(:)'

    % loc of each voxel N copy
loc= sum(logical(M0(Boxes(ii).loc,:)),2)+1;

%ind are the locations of the voxels Boxes(ii).loc(:) and the voxel copy in
%the M0 2D matrix
ind= sub2ind(size(M0),Boxes(ii).loc(:),loc(:));

% add the scale box PD values to the locations in matrix M0  
   M0(ind)=Boxes(ii).PD*Cbox(ii);
   
end

% feel the empty spots (zeors) with nan. (not all voxel end having the same number
% of estimations ).
M0(find(M0==0))=nan;
% take the PD median value for each voxel
PD_fit=nanmedian(M0,2);

%reshape PD from a vector of voxels to a 3D image.
PD_fit=reshape(PD_fit,SZ);

%Done

PD_filename=fullfile(opt.outDir, 'PD_Partical.nii.gz');
opt.PD_Partical=PD_filename;


% dtiWriteNiftiWrapper(single(PD_fit),xform,PD_filename);
PD_partical_nii = BM_nii; PD_partical_nii.img=single(PD_fit);
PD_partical_nii.hdr.dime.datatype = 64; PD_partical_nii.hdr.dime.bitpix = 64; % set header datatype to float
save_nii(PD_partical_nii, PD_filename);


save(opt.logname,'opt')

