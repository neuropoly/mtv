function mtv_batch(fname_fl,FA,TR,fname_B1)
tmpdir='mtv_DAM'; mkdir(tmpdir)
sct_unix(['sct_image -i ' fname_fl ' -setorient RPI -o ' tmpdir '/SPGRs.nii.gz'])
sct_unix(['sct_image -i ' fname_B1 ' -setorient RPI -o ' tmpdir '/B1_DAM.nii.gz'])
rmslices = 0;
r = report_generator('Report', pwd);
r.open();

cd(tmpdir)


%% 
dat = load_nii_data('SPGRs.nii.gz');
dims = size(dat);
ref=min(dims(4),3);

% crop
sct_cropXY SPGRs.nii.gz autobox 30
% segment cord
sct_unix(['fslroi SPGRs.nii SPGRs_ref.nii ' num2str(ref-1) ' 1'])
sct_unix(['fslroi SPGRs_crop.nii SPGRs_crop_ref.nii.gz ' num2str(ref-1) ' 1'])
sct_get_centerline_manual SPGRs_crop_ref.nii.gz
sct_unix(['sct_propseg -i SPGRs_crop_ref.nii.gz -t t1 -radius 6 -init-centerline SPGRs_crop_ref_centerline.nii.gz']);
%sct_unix(['fslview SPGRs_crop_ref.nii SPGRs_crop_ref_seg.nii.gz -l Red -t 0.5'])

% moco
sct_moco('SPGRs_crop.nii',ref)


% report
r.section('Preprocessing')
r.subsection('Spinal cord segmentation')
figure, imagesc(makeimagestack(squeeze(load_nii_data('SPGRs_crop_ref.nii.gz')),1,0)); colormap gray; axis image; view(-90,90);
B=bwboundaries(makeimagestack(squeeze(load_nii_data('SPGRs_crop_ref_seg.nii.gz')),0,0));
hold on; for ib = 1:length(B), plot(B{ib}(:,2),B{ib}(:,1),'r--'); end
r.add_figure(gcf,'Spinal cord segmentation using Propseg (SCT)','left');
r.end_section(); close all




%% Compute B1map
% split
% B1_LIST = sct_tools_ls('FA*.nii*');
% correct gradient non-linearity
[status, result] = unix('source /autofs/cluster/msilab/users/tools/enable_nitic.new; func.gc.nit -i B1_DAM.nii.gz');
if status, disp('ERROR!!!!!!!!!!'); disp(result); end
% addpath('/autofs/cluster/msilab/users/tools/nitic/1.1/matlab');addpath('/cluster/connectome/software/preproc/gradient_nonlin_unwarp/');
% mris_gradient_nonlin__unwarp_volume__batchmode_HCPS_v2('B1_DAM.nii.gz','B1_DAM_gc.nii.gz','coeff_AS302.grad','UNDIS','direct','0','cubic','1','1');
% sct_unix('applywarp -i B1_DAM.nii.gz -r B1_DAM.nii.gz -o B1_DAM_gc.nii.gz -w B1_DAM_deform_grad_rel.nii.gz --interp=spline');
% sct_unix('rm *rad_abs.nii.gz *bias_orig.nii.gz *volumes_warp.nii.gz *grad_rel.nii.gz');

B1_LIST = sct_splitTandrename('B1_DAM_gc.nii.gz');
sct_unix('sct_register_multimodal -i B1_DAM_gcT0000.nii.gz -d B1_DAM_gcT0001.nii.gz -o B1_DAM_gcT0000_reg.nii.gz -param step=1,type=im,algo=translation')
% Compute B1 profile
sct_unix(['fslmaths -dt double B1_DAM_gcT0001.nii.gz -div 2 -div B1_DAM_gcT0000_reg.nii.gz -acos -div ' num2str(60*pi/180) ' b1']);
[b1Map, hdr]=load_nii_data('b1.nii.gz');
% Smooth B1
mask = load_nii_data('B1_DAM_gcT0001.nii.gz'); mask = mask>0.05*max(mask(:)) & mask~=max(mask(:));
b1Map_smooth = mtv_fit3dsplinemodel(b1Map,mask,[],0.1,hdr.dime.pixdim(2:4));
save_nii_v2(b1Map_smooth,'b1_smooth.nii.gz','b1.nii.gz')

% Register B1
sct_unix('sct_register_multimodal -i B1_DAM_gcT0001.nii.gz -d SPGRs_crop_ref.nii.gz -param step=1,type=im,algo=slicereg');
unix('rm SPGRs_crop_ref_reg.nii.gz');
sct_unix('sct_apply_transfo -i b1_smooth.nii.gz -d SPGRs_crop_ref.nii.gz -w warp_B1_DAM_gcT00012SPGRs_crop_ref.nii.gz');

% REPORT
r.subsection('B1')
h = figure;
imagesc3D(double(mask).*b1Map_smooth,[.9 1.1]); title('b1Map_smooth'); view(-90,90); colorbar; drawnow;
h(2) = figure;
imagesc3D(double(mask).*b1Map,[.9 1.1]); view(-90,90); title('b1Map'); colorbar;drawnow;
r.add_figure(h,'B1 mapping','left')
close(h);
h = figure;
imagesc3D(load_nii_data('b1_smooth_reg.nii.gz'),[.9 1.1]); view(-90,90); title('B1 smooth registered to FLASH space'); colorbar;drawnow;
h(2) = figure;
imagesc3D(load_nii_data('B1_DAM_gcT0001_reg.nii.gz')); view(-90,90); colorbar;title('B1 raw data (input of registration)'); drawnow;
h(3) = figure;
imagesc3D(load_nii_data('SPGRs_crop_ref.nii.gz')); view(-90,90); colorbar;title('Flash (ref of registration)');drawnow;
r.add_figure(h,'B1 to Flash Registration','left')
close(h);
r.end_section();

%% compute T1 and M0
dat = double(load_nii_data('SPGRs_crop_moco.nii'));
nZ = size(dat,3);
B1smooth = load_nii_data('b1_smooth_reg.nii.gz');
[M0, t1] = mtv_compute_m0_t1(dat, FA, TR, B1smooth); %,[],0,1);
save_nii_v2(M0(:,:,:,1),'M0','SPGRs_crop_ref.nii.gz')
save_nii_v2(t1(:,:,:,1),'T1','SPGRs_crop_ref.nii.gz',64)


%% create csf mask
% csf mask using T1 values
csf_mask=t1(:,:,:,1);
csf_mask(csf_mask<.8 | csf_mask>5)=0;
% use cord seg
[SC_seg,hdr]=load_nii_data('SPGRs_crop_ref_seg.nii.gz');
SC_seg(:,:,[1:4 end-4:end])=0;
% csf mask using seg
SE=strel('disk',floor(4/hdr.dime.pixdim(1)),4);
seg_csf=logical(circshift(imdilate(SC_seg,SE),4,2)-SC_seg);

% compose
seg_csf=csf_mask & seg_csf;

save_nii_v2(seg_csf,'CSF_seg','SPGRs_crop_ref_seg.nii.gz');

[~, seg]=mtv_mrQ_Seg_kmeans_simple(t1(:,:,:,1),SC_seg,M0);

save_nii_v2(seg==3 | seg==2,'mask_WM','SPGRs_crop_ref_seg.nii.gz');

% Report
r.subsection('Masks')
h=figure;
imagesc(imfuse(makeimagestack(seg==3 | seg==2,0,0), makeimagestack(dat(:,:,:,ref),2,0))); view(-90,90); axis image; drawnow;
r.add_figure(h,'White Matter','left'); close(h);
h=figure;
imagesc(imfuse(makeimagestack(seg_csf,0,0), makeimagestack(dat(:,:,:,ref),2,0))); view(-90,90); axis image; drawnow;
r.add_figure(h,'CSF','left'); close(h);
r.end_section();



%% compute PD
% metrics={'M0' 'T1' 'SPGRs_crop_ref_seg' 'CSF_seg'};
% for im=1:length(metrics)
% sct_unix(['fslroi ' metrics{im} ' ' metrics{im} '_Z4_18 0 -1 0 -1 3 15']);
% end
mtv_correct_receive_profile_v2('M0.nii','T1.nii','mask_WM.nii','CSF_seg.nii',1,[1 1 2])
sct_unix(['fslroi mtv mtv_Z' num2str(rmslices+1) '_' num2str(nZ - rmslices) ' 0 -1 0 -1 ' num2str(rmslices) ' ' num2str(nZ - 2*rmslices)]);
sct_unix(['fslroi T1 T1_Z' num2str(rmslices+1) '_' num2str(nZ - rmslices) ' 0 -1 0 -1 ' num2str(rmslices) ' ' num2str(nZ - 2*rmslices)]);

% Report
r.section('Results')
h=figure;
dat = load_nii_data('M0.nii',[(rmslices+1):(nZ-rmslices)]);
imagesc3D(dat,2); view(-90,90); drawnow; colorbar;
r.add_figure(h,'M0','left'); close(h);
h=figure;
imagesc3D(load_nii_data('T1.nii.gz',[(rmslices+1):(nZ-rmslices)]),[.5 2]); view(-90,90); colormap gray; drawnow; colorbar;
r.add_figure(h,'T1','left'); close(h);
h=figure;
imagesc3D(load_nii_data('mtv.nii.gz',[(rmslices+1):(nZ-rmslices)]),[0 .35]); view(-90,90); drawnow; colorbar;
r.add_figure(h,'MTV','left'); close(h);

%% copy back
sct_unix(['cp mtv_Z*.nii.gz ../mtv.nii.gz']);
sct_unix(['cp T1_Z*.nii.gz ../T1.nii.gz']);
cd ..
