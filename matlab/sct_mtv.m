function sct_mtv(EPI, angles, SPGR, flipAngles, maskCSF, TR, niftiheaderforoutputs, verbose)

% compute B1
unix(['sct_mtv_b1angle.py ' EPI angles])

% T1 fitting with B1 correction
[M0 t1Biased] = scd_fitData_MTV (SPGR, flipAngles, TR, b1Map);
[imiZmask_median_reg, imiZmask_median] = scd_display_intensitySbS(M0,mask, verbose);

% Compute PD: normalize M0 with M0 in csf SBS
PD=scd_mtv_M0toPD(M0, maskCSF);

save_avw_v2(1-PD,'MTVF','f',[1 1 1 3], niftiheaderforoutputs, 1)

