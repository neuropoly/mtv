tmpdir=sct_tempdir;
copyfile('SPGRs.nii.gz',tmpdir)
copyfile('B1_DAM.nii.gz',tmpdir)
cd(tmpdir)
%% 
ref=2;

% crop
sct_cropXY SPGRs.nii.gz autobox 30
%moco
sct_unix(['fslroi SPGRs_crop.nii SPGRs_crop_ref.nii ' num2str(ref-1) ' 1'])
sct_moco('SPGRs_crop.nii',ref)

% segment cord
sct_get_centerline_manual SPGRs_crop_ref.nii.gz
save_nii_v2(load_nii('SPGRs_crop_ref.nii.gz'),'SPGRs_crop_ref.nii.gz')
sct_unix(['sct_propseg -i SPGRs_crop_ref.nii.gz -t t1 -radius 3 -init-centerline SPGRs_crop_ref_centerline.nii']);
sct_unix(['fslview SPGRs_crop_ref.nii SPGRs_crop_ref_seg.nii.gz -l Red -t 0.5'])



%% Compute B1map
% split
% B1_LIST = sct_tools_ls('FA*.nii*');
B1_LIST = sct_splitTandrename('B1_DAM.nii.gz');
sct_unix('sct_register_multimodal -identity 1 -x nn -i B1_DAMT0000.nii.gz -d SPGRs_crop_ref.nii.gz')
sct_unix('sct_register_multimodal -identity 1 -x nn -i B1_DAMT0001.nii.gz -d SPGRs_crop_ref.nii.gz')

% register SPGR_ref
sct_unix(['fslroi SPGRs.nii.gz SPGRs_ref.nii ' num2str(ref-1) ' 1'])

% Compute B1 profile
sct_unix(['fslmaths -dt double ' B1_LIST{2} ' -div 2 -div ' B1_LIST{1} ' -acos -div ' num2str(60*pi/180) ' b1']);
% Reslice B1 in spgr space
sct_reslice(B1_LIST{1},'SPGRs_ref.nii.gz',1)
unix('mv step0 b12SPGRspace')
sct_unix(['sct_apply_transfo -i b1.nii.gz -d SPGRs_ref.nii.gz -w b12SPGRspace/step01Warp.nii.gz'])
sct_cropXY b1_reg.nii.gz autobox 30
%sct_unix(['fslmaths ' sct_tool_remove_extension(B1_LIST{1},0) '_bgmask_reg.nii -thr 0.8 -bin b1_mask.nii'])

% remove useless files
%sct_unix('rm *_bgmask*')
%sct_unix('rm warp*')
%sct_unix('rm SPGRs_ref_reg.nii.gz')

% mask b1 based on T1<1.5s and background mask
    % create background mask
    sct_create_mask_bg_nii(B1_LIST{1},0.05)
        % warp it
        sct_unix(['sct_apply_transfo -i ' sct_tool_remove_extension(B1_LIST{1},0) '_bgmask.nii -o b1_mask_reg.nii.gz -d SPGRs_ref.nii.gz -w b12SPGRspace/step01Warp.nii.gz'])
        sct_cropXY b1_mask_reg.nii.gz autobox 30

        % load it
        mask=load_nii_data('b1_mask_reg_crop.nii');
    % estimate T1 without b1 corr
    [~, t1Biased] = mtv_compute_m0_t1(double(load_nii_data('SPGRs_crop.nii')), [4 10 20], 0.03, load_nii_data('b1_reg_crop.nii')); %,[],0,1);
    save_nii_v2(t1Biased(:,:,:,1),'T1','SPGRs_crop.nii',64);
    % combine both masks
    mask=mask>0.3 & t1Biased(:,:,:,1)<1.7;
    % save mask
    save_nii_v2(mask,'b1_mask_reg_crop.nii','SPGRs_crop.nii');

% smooth b1
mask = load_nii_data('b1_mask_reg_crop.nii'); mask(:,:,[1:4 19:end])=0;
b1Map=load_nii_data('b1_reg_crop.nii'); mask = ~~b1Map; mask(:,:,[1:4 19:end])=0;
b1Map=mtv_fit3dpolynomialmodel(b1Map,mask,6); save_nii_v2(b1Map,'b1_reg_crop_smooth_nomask','b1_reg_crop.nii')

%% compute T1 and M0
[M0, t1] = mtv_compute_m0_t1(double(load_nii_data('SPGRs_crop_moco.nii')), [4 10 20], 0.03, b1Map); %,[],0,1);
save_nii_v2(M0(:,:,:,1),'M0bis','SPGRs.nii.gz')
save_nii_v2(t1(:,:,:,1),'T1bis','SPGRs.nii.gz',64)

%% create csf mask
% csf mask using T1 values
csf_mask=t1(:,:,:,1);
csf_mask(csf_mask<2 | csf_mask>5)=0;
% use cord seg
seg_nii=load_nii('SPGRs_crop_ref_seg.nii.gz');
% csf mask using seg
SE=translate(strel('disk',floor(4/seg_nii.scales(1)),4),[0 3]);
seg_csf=logical(imdilate(seg_nii.img,SE)-seg_nii.img);
% compose
seg_csf=csf_mask & seg_csf;
figure,imagesc3D(seg_csf)
save_nii_v2(seg_csf,'CSF_seg','SPGRs_crop_ref_seg.nii.gz');


%% compute PD
% metrics={'M0' 'T1' 'SPGRs_crop_ref_seg' 'CSF_seg'};
% for im=1:length(metrics)
% sct_unix(['fslroi ' metrics{im} ' ' metrics{im} '_Z4_18 0 -1 0 -1 3 15']);
% end
mtv_correct_receive_profile M0.nii T1.nii SPGRs_crop_ref_seg.nii.gz CSF_seg.nii
sct_unix(['fslroi mtv mtv_Z4_18 0 -1 0 -1 3 15']);
sct_unix(['fslroi T1 T1_Z4_18 0 -1 0 -1 3 15']);


%% copy back
cd ..
copyfile(['tmp_*' filesep 'mtv_Z4_18.nii.gz'],'./mtv.nii.gz')
copyfile(['tmp_*' filesep 'T1_Z4_18.nii.gz'],'./T1.nii.gz')
