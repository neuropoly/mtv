#!/usr/bin/env python
#########################################################################################
#
# Compute FVF (Fiber Volume Fraction) map from FA (Fractional Anisotropy) map given in
# input using the formula derived in N. Stikov et al., NeuroImage, Volume 54, Issue 2,
# 2011, Pages 1112-1121, ISSN 1053-8119.
#
# ---------------------------------------------------------------------------------------
# Copyright (c) 2015 Polytechnique Montreal <www.neuro.polymtl.ca>
# Authors: Simon LEVY
#
# About the license: see the file LICENSE.TXT
#########################################################################################

import sys
from numpy import square
from msct_parser import Parser
from msct_image import Image
from sct_utils import printv

class Param:
    def __init__(self):
        self.verbose = 1


# PARSER
# ==========================================================================================
def get_parser():

    # Initialize the parser
    parser = Parser(__file__)
    parser.usage.set_description('Compute FVF (Fiber Volume Fraction) map from FA (Fractional Anisotropy) map given in input.')
    parser.add_option(name="-fa",
                      type_value='file',
                      description="Fractional Anisotropy map as a NIFTI file.\n",
                      mandatory=True,
                      example="fa.nii.gz")
    parser.add_option(name="-o",
                      type_value='file_output',
                      description='Output FVF map.',
                      mandatory=False,
                      example='fvf.nii.gz',
                      default_value='fvf_from_fa.nii.gz')
    parser.add_option(name="-v",
                      type_value="multiple_choice",
                      description="""Verbose. 0: nothing. 1: basic.""",
                      mandatory=False,
                      default_value=param.verbose,
                      example=['0', '1'])
    return parser


# MAIN
# ==========================================================================================
def main():

    # Get parser info
    parser = get_parser()
    arguments = parser.parse(sys.argv[1:])
    fa_fname = arguments["-fa"]
    output_fname = arguments["-o"]
    verbose = int(arguments['-v'])

    # Open input files
    fa_im = Image(fa_fname)

    # Compute g-ratio
    printv('\nCompute Fiber Volume Fraction...', verbose)
    fvf_im = fa_im
    fvf_im.data = 0.883*square(fa_im.data) - 0.082*fa_im.data + 0.074
    printv('\tDone.', verbose)

    # Save output file
    printv('\nGenerate output files...', verbose)
    fvf_im.setFileName(output_fname)
    fvf_im.save()
    printv('Created file:\n--> '+str(fvf_im.file_name+fvf_im.ext)+'\n', verbose, 'info')


# START PROGRAM
# ==========================================================================================
if __name__ == "__main__":
    # initialize parameters
    param = Param()
    # call main function
    main()
