#!/usr/bin/env python
#########################################################################################
#
# Compute g-ratio from MVF (Myelin Volume Fraction) and FVF (Fiber Volume Fraction) given
# in input using the formula derived in N. Stikov et al., NeuroImage, Volume 54, Issue 2,
# 2011, Pages 1112-1121, ISSN 1053-8119 and N. Stikov et al., NeuroImage, Volume 118,
# 2015, Pages 397-405, ISSN 1053-8119.
#
# ---------------------------------------------------------------------------------------
# Copyright (c) 2015 Polytechnique Montreal <www.neuro.polymtl.ca>
# Authors: Simon LEVY
#
# About the license: see the file LICENSE.TXT
#########################################################################################

import sys
import numpy as np
from msct_parser import Parser
from msct_image import Image
from sct_utils import printv

class Param:
    def __init__(self):
        self.verbose = 1


# PARSER
# ==========================================================================================
def get_parser():

    # Initialize the parser
    parser = Parser(__file__)
    parser.usage.set_description('Compute AVF (Axon Volume Fraction), FVF (Fiber Volume Fraction) and g-ratio from MVF (Myelin Volume Fraction) and from  NODDI or FA.')
    parser.add_option(name="-mvf",
                      type_value='file',
                      description="Myelin Volume Fraction map as a NIFTI file.\nIN ADDITION, ONE OF THE TWO ARGUMENTS \'-noddi\' or \'-fa\' IS MANDATORY.",
                      mandatory=True,
                      example="mtv.nii.gz")
    parser.add_option(name="-fa",
                      type_value='file',
                      description="Fractional Anisotropy map as a NIFTI file.\n",
                      mandatory=False,
                      default_value=None)
    parser.add_option(name="-noddi",
                      type_value=[[','], 'str'],
                      description="Isotropic volume fraction and intracellular volume fraction maps as NIFTI files.\n",
                      mandatory=False,
                      example="fiso.nii.gz,ficvf.nii.gz",
                      default_value=None)
    parser.add_option(name="-o",
                      type_value='file_output',
                      description='Output g-ratio map.',
                      mandatory=False,
                      example='gratio.nii.gz',
                      default_value='gratio.nii.gz')
    parser.add_option(name="-v",
                      type_value="multiple_choice",
                      description="""Verbose. 0: nothing. 1: basic.""",
                      mandatory=False,
                      default_value=param.verbose,
                      example=['0', '1'])
    return parser


# MAIN
# ==========================================================================================
def main():

    # Get parser info
    parser = get_parser()
    arguments = parser.parse(sys.argv[1:])
    mvf_fname = arguments["-mvf"]
    if "-fa" in arguments:
        fa_fname = arguments["-fa"]
        noddi_fnames = None
    elif "-noddi" in arguments:
        noddi_fnames = arguments["-noddi"]
        fa_fname = None
    output_fname = arguments["-o"]
    verbose = int(arguments['-v'])

    # Open input files
    mvf_im = Image(mvf_fname)

    # Compute FVF and AVF
    # in case FA was given in input
    if fa_fname:
        # Open input files
        fa_im = Image(fa_fname)

        # Compute FVF
        printv('\nCompute Fiber Volume Fraction...', verbose)
        fvf_im = fa_im.copy()
        fvf_im.data = 0.883*np.square(fa_im.data) - 0.082*fa_im.data + 0.074
        printv('\tDone.', verbose)

        # Compute AVF
        printv('\nCompute Axon Volume Fraction...', verbose)
        avf_im = fa_im.copy()
        avf_im.data = fvf_im.data - mvf_im.data
        printv('\tDone.', verbose)

        # Save output file
        printv('\nGenerate output files...', verbose)
        fvf_im.setFileName('fvf_from_fa.nii.gz')
        avf_im.setFileName('avf_from_fa.nii.gz')
        avf_im.save()
        fvf_im.save()
        printv('Created file:\n--> '+str(fvf_im.file_name+fvf_im.ext)+'\n--> '+str(avf_im.file_name+avf_im.ext)+'\n', verbose, 'info')

    # in case NODDI indices were given in input
    elif noddi_fnames:
        # Open input files
        if len(noddi_fnames) != 2:
            printv('ERROR: Two file names (separated by a comma) are required for the argument \'-noddi\': the isotropic volume fraction map and the intracellular volume fraction map.', type='error')
        fiso_im = Image(noddi_fnames[0])
        ficv_im = Image(noddi_fnames[1])

        # Compute AVF
        printv('\nCompute Axon Volume Fraction...', verbose)
        avf_im = fiso_im.copy()
        avf_im.data = np.multiply(np.multiply(np.ones(fiso_im.data.shape) - mvf_im.data, np.ones(fiso_im.data.shape) - fiso_im.data), ficv_im.data)
        printv('\tDone.', verbose)

        # Compute FVF
        printv('\nCompute Fiber Volume Fraction...', verbose)
        fvf_im = fiso_im.copy()
        fvf_im.data = avf_im.data + mvf_im.data
        printv('\tDone.', verbose)

        # Save output file
        printv('\nGenerate output files...', verbose)
        fvf_im.setFileName('fvf_from_noddi.nii.gz')
        avf_im.setFileName('avf_from_noddi.nii.gz')
        avf_im.save()
        fvf_im.save()
        printv('Created file:\n--> '+str(fvf_im.file_name+fvf_im.ext)+'\n--> '+str(avf_im.file_name+avf_im.ext)+'\n', verbose, 'info')
    else:
        printv('ERROR: You need to select one of the two arguments \'-noddi\' or \'-fa\'.', verbose, 'error')


    # Compute g-ratio
    printv('\nCompute g-ratio...', verbose)
    gratio_im = fvf_im.copy()
    gratio_im.data = np.sqrt(np.ones(np.shape(mvf_im.data)) - np.divide(mvf_im.data, fvf_im.data))
    # gratio_im.data[~isfinite(gratio_im.data)] = 0.0
    # gratio_im.data[isinf(gratio_im.data)] = 0.0
    # to reduce the effect of noisy voxels on the metric estimation process
    gratio_im.data[np.isnan(gratio_im.data)] = 0.0
    gratio_im.data[gratio_im.data < 0.0] = 0.0
    gratio_im.data[np.isposinf(gratio_im.data)] = 1.0

    printv('\tDone.', verbose)

    # Save output file
    printv('\nGenerate output files...', verbose)
    gratio_im.setFileName(output_fname)
    gratio_im.save()
    printv('Created file:\n--> '+str(gratio_im.file_name+gratio_im.ext)+'\n', verbose, 'info')


# START PROGRAM
# ==========================================================================================
if __name__ == "__main__":
    # initialize parameters
    param = Param()
    # call main function
    main()
