#!/usr/bin/env python

__author__ = 'slevy_local'

from msct_parser import Parser
import sys
from sct_utils import run, find_file_within_folder


def main():

    # Parse input
    parser = Parser(__file__)
    parser.usage.set_description('Get the scalar used to increase the dynamic range during the acquisition of a Spoiled '
                                 'Gradient Echo from the dicom field \"ScanOptions\" and normalize the NIFTI data by it'
                                 '.')
    parser.add_option(name="-nifti",
                      type_value="file",
                      description="nifti of the Spoiled Gradient Echo",
                      mandatory=True,
                      example="spgr5.nii.gz")
    parser.add_option(name="-o",
                      type_value="file_output",
                      description="output nifti file (normalized by the scalar)",
                      mandatory=True,
                      example="spgr5_norm.nii.gz")
    parser.add_option(name="-dcm",
                      type_value="folder",
                      description="DICOM folder of the Spoiled Gradient Echo image",
                      mandatory=True,
                      example="d_sp_pain_pilot4-0001.dcm")
    arguments = parser.parse(sys.argv[1:])

    fname_nifti = arguments["-nifti"]
    fname_output = arguments["-o"]
    path_dcm = arguments["-dcm"]

    fname_dcm = find_file_within_folder("*.dcm", path_dcm)[0]

    scalar = extract_scalar(fname_dcm)

    run('fslmaths '+fname_nifti+' -div '+str(scalar)+' '+fname_output)


def extract_scalar(fname_dcm):

    from dicom import read_file

    dcm = read_file(fname_dcm)

    return int(dcm.ScanOptions)

# RUN MAIN
# =========================================================================================
if __name__ == "__main__":
    main()

