#!/usr/bin/env python
__author__ = 'slevyrosetti'
#########################################################################################
#
# Smooth B1 angle scaling map resulting from low resolution EPI.
#
# ---------------------------------------------------------------------------------------
# Copyright (c) 2015 Polytechnique Montreal <www.neuro.polymtl.ca>
# Authors: Simon LEVY
#
# About the license: see the file LICENSE.TXT
#########################################################################################

import sys
import nibabel as nib
from msct_parser import *
from scipy.ndimage.filters import median_filter
from sct_utils import printv, run
from msct_image import Image
from sct_image import get_orientation_3d, set_orientation
from numpy import median, mean, percentile



# =======================================================================================================================
# main
# =======================================================================================================================
def main():

    # Check input Parameters
    parser = Parser(__file__)
    parser.usage.set_description('Smooth B1 angle scaling map resulting from low resolution EPI.')
    parser.add_option("-i", "file", "B1 angle scaling map to be smoothed.", True, "b1.nii.gz", default_value='')
    parser.add_option("-o", "str", "Output file name.", False, "b1smoothed.nii.gz", default_value='b1smoothed.nii.gz')
    parser.add_option("-size", [[','],'int'], "Kernel size of the median filter to use for smoothing.", False, example="15,15,3", default_value=[25, 25, 1])
    parser.add_option(name="-rois",
                      type_value=[[','], 'file'],
                      description='File names of the cord segmentation and of the CSF segmentation to replace the vales in the CSF by the median of the cord before smoothing. Specify as: <cord seg file name>,<CSF seg file name>.',
                      mandatory=False,
                      default_value='',
                      example='spgr20crop_seg_ok.nii.gz,spgr20crop_csf.nii.gz')
    parser.add_option(name="-v",
                      type_value="int",
                      description="""Verbose.""",
                      mandatory=False,
                      default_value=0)

    usage = parser.usage.generate()

    # Parameters for debug mode
    arguments = parser.parse(sys.argv[1:])
    fname_b1 = arguments["-i"]
    file_out = arguments["-o"]
    size_kernel = arguments["-size"]
    if "-rois" in arguments:
        rois = arguments["-rois"]
    else:
        rois =''
    if "-v" in arguments:
        verbose = arguments["-v"]
    else:
        verbose = 0

    # check if B1 map is in RPI orientation
    # Check if the orientation of the data is RPI
    b1_im = Image(fname_b1)
    b1_orientation = get_orientation_3d(b1_im)

    if b1_orientation != 'RPI':
        # If orientation is not RPI, change to RPI and load data
        printv('\nChange the orientation of the B1 map into RPI and load it...', 1)
        path_tmp = sct.tmp_create()
        b1_im = set_orientation(b1_im, 'RPI', fname_out=path_tmp + 'b1_RPI.nii')
        b1_data = b1_im.data
        # Remove the temporary folder used to change the NIFTI file orientation into RPI
        printv('\nRemove the temporary folder...', verbose)
        status, output = run('rm -rf ' + path_tmp)
    else:
        # Load image
        printv('\nLoad B1 map...', verbose)
        b1_data = b1_im.data

    printv('\tDone.', verbose)


    # load B1 map
    if rois:
        # check integrity
        if len(rois) != 2:
            printv('ERROR: Please specify correctly the file names for the cord and the CSF segmentation in flag -rois.', type='error')
        # load cord and CSF segmentations
        cord_mask = nib.load(rois[0]).get_data()
        csf_mask = nib.load(rois[1]).get_data()

        # replace CSF values by the median value within the cord slice-by-slice
        for i_z in range(0, b1_data.shape[-1]):
            b1_data[csf_mask[:, :, i_z] > 0, i_z] = percentile(b1_data[cord_mask[:, :, i_z] > 0, i_z], 75)

        # save the B1 map previously modified
        nib.save(nib.Nifti1Image(b1_data, None, b1_im.hdr), 'b1_modif.nii.gz')

    # smooth B1 map
    b1_smoothed = median_filter(b1_data, size=size_kernel)

    # Save resulting map
    nib.save(nib.Nifti1Image(b1_smoothed, None, b1_im.hdr), file_out)


# =======================================================================================================================
# Start program
# =======================================================================================================================
if __name__ == "__main__":
    # Call main function
    main()
