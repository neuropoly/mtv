This repository provides code to compute T1, B1 transmit field, PD (Proton Density) and MTV (Macromolecular Tissue Volume) maps from spinal cord spoiled gradient echo images with variable flip angles.

This repository is split in two folders:

- the folder `python` includes Python scripts to estimate B1 transmit field maps from low resolution EPI images with 60 and 120° flip angle
- the folder `matlab` includes:
     - scripts and functions to fit Inversion Recovery T1 mapping model from Joelle Barral et al.
     - all scripts and functions to compute T1, PD and MTV maps, including scripts to estimate the B1 receive profile; this code was adapted to the spinal cord from mrQ toolbox (https://github.com/mezera/mrQ) developed by Aviv Mezer et al.
     - a folder named `mtsat` providing matlab scripts to compute MTsat maps from PD-, T1- and MT-weighted images.